module.exports = {
    content: [
        "./src/**/*.{js,jsx,ts,tsx}",
    ],
    theme: {
        extend: {
            textColor: {
                'salmon': '#FF7B71',
                'grey': '#979797',

            }, fontFamily: {
                'montserrat': ['Montserrat'],

            }, backgroundColor: {
                'salmon': '#FF7B71'
            }, colors: {
                'hotpink': '#FF7DB2',
                'deepskyblue': '#00C9FF',
                'darkcyan': '#00A99D',
                'tomato': '#FC6238',
                'maroon': '#5D053A',
                'salmon': '#FF7B71'
            },
        },
    },
    plugins: [],
};