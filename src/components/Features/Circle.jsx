import React from "react";


const Circle = (props) => {

    const classes = "w-48 h-48 rounded-full font-montserrat text-white font-bold " + props.className;

    return (
        <div className={classes}>
            <div className="h-full flex justify-center">
                <span className="my-auto">
                    {props.children}
                </span>
            </div>
        </div>

    )

}

export default Circle;