import React from "react";


const Rectangle = (props) => {

    const classes = "w-72 h-28 rounded-[20px] font-montserrat text-white " + props.className;


    return (
        <div className={classes}>
            <div className="h-full flex justify-center">
                <span className="my-auto">
                    {props.children}
                </span>
            </div>
        </div>
    )


}

export default Rectangle;