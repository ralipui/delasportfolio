import React from "react";

const TwoGrid = (props) => {

    const classes = "grid grid-cols-2 space-x-10 text-left pb-6 px-14 " + props.className;

    return (
        <div className={classes}>
            {props.children}
        </div>
    )
}

export default TwoGrid;