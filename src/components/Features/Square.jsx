import React from "react";

const Square = (props) => {

    const classes = "w-[167px] h-40 rounded-[30px] font-montserrat border-4 text-gray-700 " + props.className;

    return (
        <div className={classes}>
            <div className="h-full flex">
                <span className="text-center px-4 my-auto">
                {props.children}
                </span>
            </div>
        </div>
    )
}


export default Square;