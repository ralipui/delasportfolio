import React from "react";
import linkedIn from "../../img/Linkedin.png";
import behance from "../../img/behance.png";
import dribble from "../../img/Dribble.png";

function Socials() {

    return (
        <div className="flex space-x-6 my-auto">
            <div>
                <a href="https://www.linkedin.com/in/dela-alipui-904732203/" target="_blank" rel="noreferrer"><img
                    className="w-5"
                    src={linkedIn}
                    alt="logo"/></a>
            </div>
            <div>
                <a href="https://www.behance.net/delaalipui" target="_blank" rel="noreferrer"><img className="w-6"
                                                                                                   src={behance}
                                                                                                   alt="logo"/></a>
            </div>
            <div>
                <a href="https://dribbble.com/delalipui" target="_blank" rel="noreferrer"><img className="w-5"
                                                                                               src={dribble}
                                                                                               alt="logo"/></a>
            </div>
        </div>
    )

}

export default Socials;