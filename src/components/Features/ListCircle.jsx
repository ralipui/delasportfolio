import React from "react";

const ListCircle = (props) => {

    const classes = "rounded-full w-6 h-6 " + props.className;

    return (
        <div className={classes}/>
    )
}

export default ListCircle;