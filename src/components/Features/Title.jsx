import React from "react";

const Title = (props) => {

    const classes = "font-bold text-2xl pb-8 " + props.className;

    return (
        <div className={classes}>
            {props.children}
        </div>
    )
}

export default Title;