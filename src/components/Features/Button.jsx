import React from "react";


const Button = (props) => {

    return (
        <a href={props.next_project}>
            <button
                className="bg-salmon px-10 py-5 rounded-md text-white tracking-wider font-montserrat text-md hover:bg-white hover:text-salmon outline-2 outline-salmon outline hover:outline-2 hover:outline-salmon duration-500">{props.text}
            </button>
        </a>
    )
}


const PrototypeButton = (props) => {

    return (

        <a href={props.prototypelink} target="_blank" rel="noreferrer">
            <button
                className="bg-white px-10 py-5 rounded-md text-salmon outline outline-2 tracking-wider font-montserrat text-md hover:bg-salmon hover:text-white duration-500">{props.ptext}
            </button>
        </a>

    )


}


export default Button;
export {PrototypeButton}