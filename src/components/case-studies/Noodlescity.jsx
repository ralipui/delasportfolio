import React from "react";
import Nav from "../HomePage/Nav";
import hero_img from "../../img/Noodles City/Hero-Images.jpg";
import Topsection from "../CaseStudySections/Topsection";
import Objectives from "../CaseStudySections/Objectives";
import AppConcept from "../CaseStudySections/AppConcept";
import AppConceptnoodlescity from "../case-study-contents/AppConceptContent/AppConceptnoodlescity";
import TargetAudience from "../CaseStudySections/TargetAudience";
import NoodelscityAudience from "../case-study-contents/TargetAudienceContent/NoodelscityAudience";
import Process from "../CaseStudySections/Process";
import Research from "../CaseStudySections/Research";
import ResearchNoodlesCity from "../case-study-contents/Research-and-Define/ResearchNoodlesCity";
import ideate_img_1 from "../../img/Noodles City/crazy_img.jpg";
import ideate_img_2 from "../../img/Noodles City/story_img.jpg";
import Ideate from "../CaseStudySections/Ideate";
import DesignFramework from "../CaseStudySections/DesignFramework";
import Strategy from "../case-study-contents/DesignFramework/Strategy";
import Outline from "../case-study-contents/DesignFramework/Outline";
import outline_img from "../../img/Noodles City/Outline_of_Scope.jpg";
import InfoArchitecture from "../case-study-contents/DesignFramework/InfoArchitecture";
import sitemap from "../../img/Noodles City/Sitemap.jpg";
import Design from "../case-study-contents/DesignFramework/Design";
import mood_board from "../../img/Noodles City/Moodboard.jpg";
import logo_design from "../../img/Noodles City/Logo.jpg";
import style_guide from "../../img/Noodles City/Style_Guide.jpg";
import lowFi_img from "../../img/Noodles City/lowfi_img.png";
import PaperWireframesMain from "../case-study-contents/Prototype/PaperWireframesMain";
import pwireFrame_A from "../../img/Noodles City/pwireframe_A.jpg";
import pwireFrame_B from "../../img/Noodles City/pwireframe_B.jpg";
import Prototype from "../CaseStudySections/Prototype";
import dropdown from "../../img/Noodles City/dropdown.png";
import new_order from "../../img/Noodles City/New_order.png";
import high_fidelity from "../../img/Noodles City/visual_mockup.png";
import TestSection, {ThirdPart, VisualMockups, NormalAccessibility, AccImage} from "../CaseStudySections/TestSection";
import mobile_money from "../../img/Noodles City/Mobile_Money.png";
import acc_img from "../../img/Noodles City/accessibility_img.png";
import FinalSection, {Takeaways} from "../CaseStudySections/FinalSection";
import Contact from "../HomePage/Contact";
import Button, {PrototypeButton} from "../Features/Button";

const Noodlescity = () => {

    const listItems = ["Provide an online food ordering system that is user-friendly.", "For users to have easy access to affordable food online.", "For users to have a satisfying experience that will bring them back for more."];
    const user_needs = ["Find out if the restaurant delivers to their area", "Order food online", "Customize their order", "Find out the modes of payment (credit card or pay on delivery)", "Find out the foods available", "View their past orders", "Find out the prices of the food available", "Find out estimated delivery time", "Find out the working hours and days", "Find out contact information", "Create an account", "Pay online"];
    const client_needs = ["To sell food online that will be delivered", "Easily make updates", "Increase orders", "Provide a system for order customization", "Increase efficiency of the delivery system", "Communicate reliability"];

    return (
        <section>
            <Nav/>
            <Topsection main_img={hero_img} title="Welcome to Noodles City"
                        description="Noodles City is an online restaurant that provides affordable customizable noodles to the people of East Legon, especially the working class and students. The website includes a custom menu that allows customers to make their preferred noodles from a wide range of proteins, veggies, and sauces."
                        course="Course"
                        course_desc="UI/UX Specialization (CALARTS)" project="Project Duration"
                        duration="December 2020" role="My Role" the_role="UX Designer" tools="Tools"
                        the_tools="Adobe XD and Adobe Illustrator"
                        prototypeButtonA={<PrototypeButton ptext="View Protoype"
                                                           prototypelink="https://xd.adobe.com/view/f1d2301d-7be8-412e-85f9-d86536a8ccf9-a481/"/>}/>

            <Objectives objtitle="Mymedic aims:"
                        problem="The restaurant only delivers to customers in East Legon, which is a large town in Accra, Ghana. I chose East Legon because a lot of people live there, and houses a lot of thriving businesses,
                         hence employees and employers would be looking for good and affordable meals in the area. East Legon is also the home of quite a number of universities such as Lancaster University Ghana, Webster University and Radford University,
                          thus the students and staff of the respective schools actively search for good food in the area. The problem with East Legon is that most of the residents are from the upper class. And because of this, almost everything that is sold in East Legon is expensive, especially food."
                        goals="The goal is to ensure that the customer has easy access to affordable noodles online. The customer should also be able to have a smooth ordering experience which will cause the customer to come back for more. In addition to the custom menu, customers also have the option of a classic menu to order already customized noodles,
                         and a soft drinks menu to add to their noodles order.">

                {listItems.map((listItem, index) => {
                    return <li key={index}>{listItem}</li>
                })}

            </Objectives>

            <AppConcept>
                <AppConceptnoodlescity/>
                <hr/>
            </AppConcept>

            <TargetAudience color="text-tomato" gender="More men than women"
                            education="College education or higher education"
                            occupation="Working class, college students"
                            age="18 - 35 years old"
                            location="East Legon, Accra, Ghana"
                            maritalStatus="More single people than married people"
                            income="Limited income"
                            personality="youthful, independent, adventurous, picky, hardworking."
                            values="open-mindedness, spontaneity, efficiency, determination, optimism."
                            lifestyle="family oriented, studious, active, always out with friends, Trying out new things.">
                <NoodelscityAudience/>
            </TargetAudience>

            <Process color="text-tomato" arrowColor="#FC6238"
                     research="User research, Personas, Journey maps and Competitive audit"
                     ideate="Crazy 8's and Storyboards"
                     framework="Strategy, Outline of scope, Information architecture and Strategy Process"
                     proto="Paper wireframes, Low fidelity wireframes, and prototype. Visual mockups and high fidelity prototype."
                     test="Usability test"
            />

            <Research>
                <ResearchNoodlesCity color="text-tomato"
                                     research="During my user research process, I conducted interviews to understand people’s ordering behavior; why and how they do it. I also wanted to know their views on my concept for Noodles City and what they would add, subtract or maintain from the idea. I interviewed 4 participants between the ages of 18 and 35 years old. The participants included 2 students, and 2 workers. This was to make sure that I could understand the age group that the website is targeted at so I could design a website to meet their needs. "/>

            </Research>

            <Ideate color="text-tomato mb-6" crazy_img={ideate_img_1} story_img={ideate_img_2} storyboard="Close Up Storyboard"
                    story_content="I created a close up storyboard to show how users would use the Noodles City website. The close up storyboard focuses on the product rather than the user.">

                <div className="mb-2">Out of the 8 ideas, 2 stood out to me:</div>

                <div><span className="font-bold text-black leading-relaxed">The third image:</span> Users can order for the other ingredients (proteins, veggies, sauces),
                                                                                                    without
                                                                                                    ordering for noodles.
                </div>

                <div><span className="font-bold text-black leading-relaxed">The sixth image:</span> Users can view the nutrition facts of their customized noodles.
                </div>
            </Ideate>

            <DesignFramework>

                <Strategy color="text-tomato" user_needs_txt="The website needs to enable the user to:" client_needs_txt="The website should enable the client to:"
                          user_needs={user_needs.map((user_need, index) => {
                              return (
                                  <li key={index}>{user_need}</li>
                              )
                          })}
                          client_needs={client_needs.map((client_need, index) => {
                              return (
                                  <li key={index}>{client_need}</li>
                              )
                          })}

                />


                <Outline
                    outline_txt="The outline of scope consists of the content and functionality requirements. The content requirements show what the users expect to see on the website. And the functionality requirement shows what tasks the user can perform."
                    outline_img={outline_img}
                />
                <InfoArchitecture
                    arch_info="Below is a sitemap of noodles city which shows the structure of the website and how it will function."
                    sitemap={sitemap}
                />
                <Design
                    design_info="This part of my project focuses on the UI of the website; it focuses on the look and feel of the website. This is achieved with the use of a color scheme, typography, and images. The design part of my work will show the breakdown of the three mentioned elements as well as a breakdown of the logo design for the website, and the style guide."
                    mood_info="My mood board displays the logos, typography, color scheme, and images that inspired my work."
                    mood_img={mood_board}
                    logo_design_A={logo_design}
                    logo_process="As stated earlier, Noodles City is a restaurant that sells affordable noodles with college students being its main target audience. So, I wanted to create a fun and playful atmosphere like fast-food restaurants such as KFC, McDonalds, etc. which can be seen on my mood board. This explains why I opted for the orange and blue colors and the Forte font type for the noodles since that is the focus. I thought of going for the cliche mustard, black and white colors but I thought of the informal and fast-food nature of the restaurant."
                    style_guide={style_guide}
                />

            </DesignFramework>

            <Prototype
                color="text-tomato"
                protoInfo="The images below show a sketch of the homepage, location, menu, cart and payment pages. I did this when I started thinking about the structure of my website."
                lowFi="The image below shows digital wireframes of the paper wireframes I sketched. But the digital wireframes also include the other pages of the website. The wireframes would be used during the first usability test."
                lowfi_img={lowFi_img}>

                <PaperWireframesMain
                    pwireFrame_A={pwireFrame_A}
                    pwireFrame_B={pwireFrame_B}
                />

            </Prototype>

            <TestSection color="text-tomato" iconColor="#FC6238"
                         testInfo="I conducted a usability study to determine user needs when using the website to order for a product, and also they use Buddy to search for a product. During this process, I created a research plan which included KPI's, prompts, research goals, amongst others."
                         lengthInfo="10 - 15 minutes"
                         partInfo="4 participants"
                         UsabilityInfo="This part of my project shows the problems participants had when ordering for food using the website, and the changes I made based on their problems."
                         firstImgheading='Dropdown button for editing order'
                         firstImginfo1="2 users pointed out that it frustrating that they could not edit their first order unless they viewed their cart before they could select their Order 1 to edit it."
                         firstImginfo2='To solve the problem, I added a dropdown button by the Order 2. This would allow users to view all their orders and would also  make it easier to select the an order and edit it.'
                         firstImg={dropdown}
                         secondImg={new_order}
                         secondImginfo1="3 participants had a problem with the fact that they there was no way of adding a new order."
                         secondImginfo2='To solve this problem, I added a "New Order" button that would allow users to add a new order.'
                         secondImgheading="Option of adding a new order"
            >

                <ThirdPart thirdImgheading="Mobile money option (mode of payment)"
                           thirdImginfo1="All participants pointed out that there was no mobile money option. Mobile money, also known as MoMo in Ghana, is a service that allows you to receive, send or save money on your mobile phone via your sim card."
                           thirdImginfo2="I solved this issue by adding the the mobile money option."
                           thirdImg={mobile_money}/>

                <VisualMockups
                    visual_mockup_info="After the tests and iterations, I brought my website to life by including visual elements; typography, icons, color, amongst others."
                    highFidelitymockup={high_fidelity}
                    testInfo1="When I was done with the design, I created a prototype and asked 4 people to perform a task by placing an order. And to test the functionality of the site, I asked them to explore the website by viewing whatever they wanted to."
                    testInfo2="I mostly received positive responses with the participants stated that they liked the look of the website with one stating that the colors used were refreshing. However, one participant had a problem with the custom menu stating that he did not like the fact that he had to scroll for long."
                    prototypeButton={<PrototypeButton ptext="View Protoype"
                                                      prototypelink="https://xd.adobe.com/view/f1d2301d-7be8-412e-85f9-d86536a8ccf9-a481/"/>}
                />

                <NormalAccessibility
                    accessibilityConsiderations="To make the Noodles City website accessible, users would be able to use the keyboard to navigate the pages of the website. The image below shows  annotations that indicate the order in which they would be able navigate the homepage."
                />

                <AccImage
                    acc_img={acc_img}
                />


            </TestSection>

            <FinalSection next_project_button={<Button text="Next Project" next_project="/tradecraft"/>} takeaways={<Takeaways
                takeaway_info="This was my second project and my first website design. I had a blast doing this project and it has taught me a lot about website design. One thing I learned from this project is that it is important that you do what you enjoy and you enjoy what you do. Once what you are doing is not damaging to you and you love it, go for it. If you are doing a job you love or a project you enjoy, it shows by the amount of work and dedication you put into it. This project definitely taught me that and it just made me fall more and more in love with UI/UX design."/>}
                          nextsteps="Conduct more user research, usability studies and make a few changes to the visual design of the website. Also improve the designs of the other pages of the website (contact, about us, FAQs, etc)."
            />

            <Contact/>


        </section>
    )
}

export default Noodlescity;