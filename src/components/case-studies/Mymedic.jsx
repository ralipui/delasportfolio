import React from "react";
import Topsection from "../CaseStudySections/Topsection";
import Nav from "../HomePage/Nav";
import hero_img from "../../../src/img/Mymedic/Hero-Images.jpg"
import Objectives from "../CaseStudySections/Objectives";
import AppConcept from "../CaseStudySections/AppConcept";
import AppConceptmymedic from "../case-study-contents/AppConceptContent/AppConceptmymedic";
import TargetAudience from "../CaseStudySections/TargetAudience";
import MymedicAudience from "../case-study-contents/TargetAudienceContent/MymedicAudience";
import Process from "../CaseStudySections/Process";
import Research from "../CaseStudySections/Research";
import ResearchMedic from "../case-study-contents/Research-and-Define/ResearchMedic";
import IdeateB from "../case-study-contents/IdeateContent/IdeateB";
import ideate_img_1 from "../../img/Mymedic/crazy_img.jpg";
import Outline from "../case-study-contents/DesignFramework/Outline";
import outline_img from "../../img/Mymedic/Outline_of_Scope.jpg";
import InfoArchitecture from "../case-study-contents/DesignFramework/InfoArchitecture";
import sitemap from "../../img/Mymedic/Mymedic_Sitemap.jpg";
import Design from "../case-study-contents/DesignFramework/Design";
import mood_board from "../../img/Mymedic/Moodboard.png";
import logo_design from "../../img/Mymedic/Logo.jpg";
import style_guide from "../../img/Mymedic/Style_guide.jpg";
import DesignFramework from "../CaseStudySections/DesignFramework";
import Features from "../case-study-contents/DesignFramework/Features";
import Prototype from "../CaseStudySections/Prototype";
import lowFi_img from "../../img/Mymedic/lowfi_img.png";
import PaperWireframesMain from "../case-study-contents/Prototype/PaperWireframesMain";
import pwireFrame_A from "../../img/Mymedic/pwireframe_A.jpg";
import pwireFrame_B from "../../img/Mymedic/pwireframe_B.jpg";
import TestSection from "../CaseStudySections/TestSection";
import track_health_img from "../../img/Mymedic/Track_your_health.png";
import voluntary_profile_img from "../../img/Mymedic/Voluntary_info.png";
import option_img from "../../img/Mymedic/Other_option.png";
import skip_the_process from "../../img/Mymedic/Skip_the_process.png";
import high_fidelity from "../../img/Mymedic/high_fidelity.png";
import {ThirdPart, FourthPart, VisualMockups, NormalAccessibility} from "../CaseStudySections/TestSection";
import FinalSection, {Takeaways} from "../CaseStudySections/FinalSection";
import Contact from "../HomePage/Contact";
import Button, {PrototypeButton} from "../Features/Button";


const Mymedic = () => {

    const listItems = ["To primarily give users an easier way to track their medications.", "To afford users the opportunity to check other related health matters."];
    const features_a = ["Medicine tracker.", "Medicine and refill reminders.", "Notifications and alarms.", "Family and caregiver support.", "Sharing of health data with significant individuals.", "Syncing your app with another user.", "Creating multiple profiles.", "Syncing multiple profiles.", "Appointments, calendars and planners.", "Drug-to-drug interaction checker.", "Treatment and pill organizer.", "Notifications and reminders for other people.", "Medication history.", "Sync reminders with wearables.", "Personalization settings."]
    const features_b = ["Add others to personal account.", "Drug expiration reminders.", "Pharmacy services and emergency call center.", "Medical information and resources.", "Gamification", "Online consultation", "Tracking other health related matters:"]
    const features_c = ["Blood pressure​", "Weight", "Water intake", "Food and calories", "Workout and exercise", "Heart rate", "Step counter / pedometer"];


    return (
        <section>
            <Nav/>
            <Topsection main_img={hero_img} title="Welcome to mymedic"
                        description="Mymedic is a mobile app that focuses on helping the Ghanaian people manage their medications. Dosages that are administered by health care providers can be stored in the app for better management."
                        course="Course"
                        course_desc="Google UX Strategy" project="Project Duration"
                        duration="May 2021 - June 2021" role="My Role" the_role="UX Designer" tools="Tools"
                        the_tools="Figma and Adobe Illustrator"
                        prototypeButtonA={<PrototypeButton ptext="View Protoype"
                                                           prototypelink="https://www.figma.com/proto/1EHt8OcrQMmruFwQikdSRr/Google-Project-3?page-id=1%3A3&node-id=1%3A6&viewport=810%2C206%2C0.11693775653839111&scaling=scale-down&starting-point-node-id=1%3A6"/>}
            />

            <Objectives objtitle="Mymedic aims:"
                        problem="Poor medication adherence is a problem in Ghana due to several factors. Medication adherence is the extent to which patients correctly take their medications and follow medical advice. Based on my experience and observations,
                        some people are not consistent with their medication because they forget or they become too occupied. This is very common with the elderly. Research on medication adherence in Ghana would be touched on in the research and define section of the case study."
                        goals="Strategy a mobile app that allows users to track their medication.">

                {listItems.map((listItem, index) => {
                    return <li key={index}>{listItem}</li>
                })}
            </Objectives>

            <AppConcept>
                <AppConceptmymedic/>
                <hr/>
            </AppConcept>

            <TargetAudience color="text-hotpink" gender="Both men and women"
                            education="High school, college or higher education"
                            occupation="Working class, college students"
                            age="15 and above"
                            location="Ghana, West Africa"
                            maritalStatus="Married and single people"
                            income="All ranges"
                            personality="generous, kind, impatient, pessimistic, disorderly, immature."
                            values="balance, determination, growth, happiness, wealth, success."
                            lifestyle="healthy living, active or fitness, workaholic, family focused.">
                <MymedicAudience/>
            </TargetAudience>

            <Process color="text-hotpink" arrowColor="#FF7DB2"
                     research="User research, Personas, Journey maps and Competitive audit"
                     ideate="Crazy 8's and How Might We"
                     framework="Features, Outline of scope, Information architecture and Strategy Process"
                     proto="Paper wireframes, Low fidelity wireframes, and prototype. Visual mockups and high fidelity prototype."
                     test="Usability test"
            />

            <Research>
                <ResearchMedic color="text-hotpink"/>

            </Research>

            <IdeateB color="text-hotpink mb-6" rect_color="bg-hotpink" crazy_img={ideate_img_1}
                     rectangle1="make the app more helpful when it comes to health information?"
                     rectangle2="add already existing profiles?" rectangle3="improve caregiver support?"
                     rectangle3_content="The app would allow users to to sync their reminders and notifications with their caregivers which would in turn allow caregivers to also
                            receive the same reminders and notifications."
                     rectangle2_content="The app would allow users to sync already existing profiles on without creating a new ones."
                     rectangle1_content="Include a search process that is linked to a search engine. This would allow users to adequate results when they search for anything.">

                <div className="mb-2">Out of the 8 ideas, 2 stood out to me:</div>

                <div><span className="font-bold text-black">The third image:</span> The app would provide users with meal options based on their medications.</div>

                <div><span className="font-bold text-black">The eight image:</span> Users are rewarded when they successfully finishing their medications.
                </div>

            </IdeateB>
            <DesignFramework>

                <Features
                    color="text-hotpink" features_a={features_a.map((feature_a, index) => {
                    return <li key={index}>{feature_a}</li>
                })}
                    features_b={features_b.map((feature_b, index) => {
                        return <li key={index}>{feature_b}</li>
                    })}
                    features_c={features_c.map((feature_c, index) => {
                        return <li key={index}>{feature_c}</li>
                    })}


                />
                <Outline
                    outline_txt="The outline of scope consists of the content and functionality requirements. The content requirements show what the users expect to see on the app. And the functionality requirement shows what tasks the user can perform."
                    outline_img={outline_img}
                />
                <InfoArchitecture
                    arch_info="The information architecture of my project focuses on the sitemap. This part of my work would serve as the foundation for my wireframes."
                    sitemap={sitemap}
                />
                <Design
                    design_info="This part of my project focuses on the look and feel of my app. It includes the logo design process, my mood board, and the mymedic style guide and UI kit."
                    mood_info="My mood board displays the logos, typography, color scheme, and images that inspired my work."
                    mood_img={mood_board}
                    logo_design_A={logo_design}
                    logo_process="The logo design of mymedic was not really a process because I just thought of what I wanted and I designed exactly that. First of all, I knew I wanted a name logo, so, all I had to do was get a name which I thought of in less than 10 minutes. Secondly, I knew that I wanted the logo color to be a gradient of the app colors, light blue and pink. And when I applied the gradient, I got exactly what I imagined. The only issue I had was the font type. For this part, all I did was to try out various font types and I finally landed on Franklin Gothic Heavy which I was very pleased with."
                    style_guide={style_guide}
                />

            </DesignFramework>

            <Prototype
                color="text-hotpink"
                protoInfo="Before designing the digital wireframes, I made paper wireframes of the screens to have an idea of what my final design would look like. The images below show five iterations of just the home screen and the sixth sketch indicates the final design for the home screen that include elements from the other sketches."
                boldProtoInfo=" Stars were used to mark the elements of each sketch that would be used in the initial digital wireframes."
                lowFi="The image below shows digital wireframes of the paper wireframes I sketched. But the digital wireframes also include the other pages of the website. The wireframes would be used during the first usability test."
                lowfi_img={lowFi_img}>

                <PaperWireframesMain
                    pwireFrame_A={pwireFrame_A}
                    pwireFrame_B={pwireFrame_B}
                />

            </Prototype>


            <TestSection color="text-hotpink" iconColor="#FF7DB2" testInfo="I conducted a usability study to determine user needs when adding a first medication and if the other screens of the app are user friendly. During
                    this process, I created a research plan which included KPI's, prompts, research goals, amongst others." lengthInfo="10 - 15 minutes"
                         partInfo="4 participants" UsabilityInfo="This part of my project shows the problems participants had when using the app, and the changes I
                                                                       made based on their problems." firstImgheading="Track your health"
                         firstImginfo1="1 participant asked if the app only tracks medications because that is all they see." firstImginfo2="To solve this problem I added the other tracking options to the home screen. This would give users
                                                                      an easy way of knowing what the app provides." firstImg={track_health_img}
                         secondImg={voluntary_profile_img} secondImginfo1="Participants had a problem with the fact that they had to enter their profile information before
                                                                      they could proceed." secondImginfo2="I solved the issue by adding a note at the end that states that the information is
                                                                      voluntary." secondImgheading="Voluntary profile information"
            >
                <ThirdPart thirdImgheading="'Other' option"
                           thirdImginfo1="3 participants addressed the fact that some medication questions do not include 'other' as an
                                                                      option. And this is a problem because some of their choices are not part of the list."
                           thirdImginfo2="To solve this issue, I included the 'Other' option at the end of any list that
                                                                                           requires it." thirdImg={option_img}/>

                <FourthPart fourthImg={skip_the_process}
                            fourthImgheading="Skip the process" fourthImginfo1="2 participants stated that there is no option to skip the process of adding a first medication when
                                                                      they have already started the process. This is a problem because sometimes users might change their
                                                                      minds about adding a medication." fourthImginfo2="My solution to this problem was to add a 'Skip' option at the top right corner of the screen which
                                                                      would take users to the home screen."/>

                <VisualMockups
                    visual_mockup_info="After the tests and iterations, I brought my app to life by including visual elements; typography,
                                                                       icons, color, amongst others."
                    highFidelitymockup={high_fidelity}
                    testInfo1="After fully designing the app, I conducted a second usability test to make sure that the app fully
                                                                       meets the needs of users. I used the same participants from the first usability test to conduct my
                                                                       second round of tests."
                    testInfo2="I mainly received positive feedback about the visual design of the app user experience. However,
                                                                    one participant pointed out that it would be nice to see the app with a dark theme especially for
                                                                       users that do not want the white theme."
                    prototypeButton={<PrototypeButton ptext="View Protoype"
                                                      prototypelink="https://www.figma.com/proto/1EHt8OcrQMmruFwQikdSRr/Google-Project-3?page-id=1%3A3&node-id=1%3A6&viewport=810%2C206%2C0.11693775653839111&scaling=scale-down&starting-point-node-id=1%3A6"/>}

                />

                <NormalAccessibility
                    accessibilityConsiderations="In order for for the app to be accessible to everyone, accessibility settings are added to the
                                                                       app's settings, providing users with a wide range of accessibility options. The app would also
                                                                       feature any accessibility option that is activated in the phone's settings."
                />
            </TestSection>

            <FinalSection next_project_button={<Button text="Next Project" next_project="/silverbird"/>} takeaways={<Takeaways takeaway_info="In conclusion, the needs of users were met through the design of this project. I was able to
                                                                    design the main part
                                                                    of
                                                                    the problem
                                                                    which is the
                                                                    ability to track medications. Few people that have tested the app are very pleased. I have learned
                                                                    a lot from
                                                                    this
                                                                    project but most
                                                                    importantly,
                                                                    I have learned that a lot can be done through technology for social good."/>}
                          nextsteps="I hope to design the dark them version of the app; it would be great to challenge myself and see
                                                                       how
                                                                       I can successfully create the dark version with the app's color theme. As usual, I will conduct
                                                                       another round of user research and usability studies since it would be one of the best way to
                                                                       update
                                                                       the app and meet user needs. And for my next rounds of testing, I would use more participants.">

                <div className="px-20 font-montserrat flex flex-col mb-24">
                    <div className="font-bold text-lg mb-12 mt-24">References</div>
                    <div className="text-gray-700 w-3/6">Atinga, R. A., Yarney, L., & Gavu, N. M. (2018, March 28). Factors influencing long-term <div
                        className="ml-12">medication non-adherence among diabetes and hypertensive patients in Ghana: A qualitative investigation. Retrieved from
                                          https://pubmed.ncbi.nlm.nih.gov/29590156/</div>
                    </div>


                    <div className="text-gray-700 w-3/6 mt-16">Boima, V., Ademola, A. D., Odusola, A. O., Agyekum, F., Nwafor, C. E., Cole, H., . . . <div
                        className="ml-12">Tayo, B. O. (2015, October 05). Factors Associated with Medication Nonadherence among Hypertensives in Ghana and Nigeria.
                                          Retrieved from https://www.hindawi.com/journals/ijhy/2015/205716/</div>
                    </div>

                </div>
            </FinalSection>

            <Contact/>

        </section>
    )
}

export default Mymedic;