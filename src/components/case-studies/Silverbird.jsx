import React from "react";
import Nav from "../HomePage/Nav";
import hero_img from "../../img/Silverbird/Hero-Images.jpg";
import Topsection from "../CaseStudySections/Topsection";
import Objectives from "../CaseStudySections/Objectives";
import AppConcept from "../CaseStudySections/AppConcept";
import AppConceptsilverbird from "../case-study-contents/AppConceptContent/AppConceptsilverbird";
import Process from "../CaseStudySections/Process";
import Research from "../CaseStudySections/Research";
import ResearchSilverbird from "../case-study-contents/Research-and-Define/ResearchSilverbird";
import EmpathSilverbird from "../case-study-contents/ImagesFiles/EmpathSilverbird";
import ideate_img_1 from "../../img/Silverbird/crazy_img.jpg";
import ideate_img_2 from "../../img/Silverbird/story_img.jpg";
import Ideate from "../CaseStudySections/Ideate";
import Strategy from "../case-study-contents/DesignFramework/Strategy";
import DesignFramework from "../CaseStudySections/DesignFramework";
import Outline from "../case-study-contents/DesignFramework/Outline";
import Design from "../case-study-contents/DesignFramework/Design";
import InfoArchitecture from "../case-study-contents/DesignFramework/InfoArchitecture";
import outline_img from "../../img/Silverbird/Outline_of_Scope.jpg";
import sitemap from "../../img/Silverbird/Silverbird_Cinema_Sitemap.jpg";
import mood_board from "../../img/Silverbird/Mood_board.png";
import logo_design from "../../img/Silverbird/Logo_Design_Process.jpg";
import other_design from "../../img/Silverbird/S.png";
import design_set from "../../img/Silverbird/Design-Set.jpg";
import Prototype from "../CaseStudySections/Prototype";
import lowFi_img from "../../img/Silverbird/lowfi_img.png";
import PaperWireframesMain from "../case-study-contents/Prototype/PaperWireframesMain";
import pwireFrame_A from "../../img/Silverbird/pwireframe_A.jpg";
import pwireFrame_B from "../../img/Silverbird/pwireframe_B.jpg";
import log_in_option from "../../img/Silverbird/Log_in_option.png";
import close_option from "../../img/Silverbird/Close_option.png";
import high_fidelity from "../../img/Silverbird/Visual_mockup.png";
import TestSection, {VisualMockups, NormalAccessibility} from "../CaseStudySections/TestSection";
import FinalSection, {Takeaways} from "../CaseStudySections/FinalSection";
import Contact from "../HomePage/Contact";
import Button, {PrototypeButton} from "../Features/Button";


const Silverbird = () => {

    const listItems = ["To give customers an easier way to purchase movie tickets.", "To give customers an easier way to reserve movie tickets."];
    const client_needs = ["Manually add and delete movies.", "Manually add, delete and edit movie details.", "Manually edit other relevant information such as contact info, about, etc.", "Increase orders."]
    const user_needs = ["Login/Logout/Signup", "Use the app without creating an account", "View movie categories", "Order food online", "Purchase a ticket", "Reserve a ticket", "Receive reminders and notifications", "Read movie news and articles", "Customize app in settings", "View movie details", "Watch movie trailers", "Use filters to search for movies", "Create a favorites list"];


    return (
        <section>
            <Nav/>
            <Topsection main_img={hero_img} title="Welcome to Silverbird Cinema"
                        description="Silverbird Cinema is a movie theatre that has locations in both Ghana and Nigeria. The cinemas are mainly located in malls and they offer customers a wide range of movies, both local and foreign."
                        course="Course"
                        course_desc="Google UX Strategy" project="Project Duration"
                        duration="April 2021 - May 2021" role="My Role" the_role="UX Designer" tools="Tools"
                        the_tools="Figma and Adobe Illustrator"
                        prototypeButtonA={<PrototypeButton ptext="View Protoype"
                                                           prototypelink="https://www.figma.com/proto/k2InuXouWdY9PnLzE7Bh20/Google-Project?page-id=155%3A4&node-id=155%3A5&viewport=324%2C-19%2C0.20972009003162384&scaling=scale-down"/>}
            />

            <Objectives objtitle="Silverbird aims:"
                        problem="Silverbird Cinema has a website that allows customers to know the movies that are currently showing or are coming out soon, movie details such as the synopsis,
                        cast, showing dates and times, amongst others. But the problem is that the website does not allow customers to purchase movie tickets. The only way customers can purchase movie tickets is at the cinema and this can be inconvenient in so many ways."
                        goals="Strategy an app that would allow users to purchase and reserve movie tickets from any location.">

                {listItems.map((listItem, index) => {
                    return <li key={index}>{listItem}</li>
                })}

            </Objectives>

            <AppConcept>
                <AppConceptsilverbird/>
                <hr/>
            </AppConcept>


            <Process color="text-deepskyblue" arrowColor="#00C9FF"
                     research="User research, Personas, Journey maps and Competitive audit"
                     ideate="Crazy 8's and Storyboards"
                     framework="Strategy, Outline of scope, Information architecture and Strategy Process"
                     proto="Paper wireframes, Low fidelity wireframes, and prototype. Visual mockups and high fidelity prototype."
                     test="Usability test"
            />

            <Research>
                <ResearchSilverbird color="text-deepskyblue" affinity={<EmpathSilverbird/>}
                                    research="For my user research, I conducted interviews to understand users experiences with the Silverbird Cinema website. I interviewed 5 participants; 2 males and 3 females between the ages of 18 and 35. This was a deliberate choice to target the main audience I stated earlier.
                      After completing the interview, I grouped the responses of the 5 participants using an empathy map to identify what users feel, think, hear, see, say and do about using the Silverbird Cinema website. Below is an empathy map of the 5 participants"/>

            </Research>

            <Ideate color="text-deepskyblue mb-6" crazy_img={ideate_img_1} story_img={ideate_img_2} storyboard="Close Up Storyboard"
                    story_content="I created a close up storyboard to show how users would use the Silverbird Cinema app to purchase a movie ticket. The close up storyboard focuses on the product rather than the user.">

                <div className="mb-2">Out of the 8 ideas, 3 stood out to me:</div>

                <div><span className="font-bold text-black leading-relaxed">The third image:</span> Users can split the cost of a ticket between themselves.
                </div>

                <div><span className="font-bold text-black leading-relaxed">The fifth image:</span> Offers loyal customers with the ‘Friend Discount’ which allows the
                                                                                                    customer to get a free ticket for a plus one.
                </div>

                <div><span className="font-bold text-black leading-relaxed">The eighth image:</span> Users can stream movies showing at the cinema.
                </div>
            </Ideate>

            <DesignFramework>

                <Strategy color="text-deepskyblue" user_needs_txt="The app needs to enable the user to:" client_needs_txt="The app should enable the client to:"
                          client_needs={client_needs.map((client_need, index) => {
                              return (
                                  <li key={index}>{client_need}</li>
                              )
                          })}
                          user_needs={user_needs.map((user_need, index) => {
                              return (
                                  <li key={index}>{user_need}</li>
                              )
                          })}
                />


                <Outline
                    outline_txt="The outline of scope consists of the content and functionality requirements. The content requirements show what the users expect to see on the app. And the functionality requirement shows what tasks the user can perform."
                    outline_img={outline_img}
                />
                <InfoArchitecture
                    arch_info="The image below shows the Silverbird Cinema app sitemap"
                    sitemap={sitemap}
                />
                <Design
                    design_info="The design part of my project focuses on the UI design process of the app. The UI process provides a breakdown of the colors, font type, and images used to contribute to the look and feel of the app. This part of my work also includes the Silverbird Cinema app style guide."
                    mood_info="My mood board displays the logos, typography, color scheme, and images that inspired my work."
                    mood_img={mood_board}
                    logo_design_A={logo_design}
                    logo_design_B={other_design}
                    logo_process="Silverbird Cinema already has a logo but I chose to redesign the logo to fit the look and feel I had envisioned for the app. I decided to maintain the 'S' and the blue circle that represents the Silverbird but it more modern. I also changed the blue shade of the logo to a brighter shade so it stands out. During my logo design process, I played around with the letter 'S' and the circle. This made it easier to keep the logo simple and close to the original design."
                    style_guide={design_set}
                />

            </DesignFramework>

            <Prototype
                color="text-deepskyblue"
                protoInfo="For this part of my project, I designed paper wireframes of a few screens for the app. I made sketches of each screen and made sure to include elements that would make the app user friendly. The images below show five iterations of the home screen and the sixth sketch indicates the final design for the home screen that include elements from the other sketches."
                boldProtoInfo=" Stars were used to mark the elements of each sketch that would be used in the initial digital wireframes."
                lowFi="The image below shows digital wireframes of the paper wireframes I sketched. The initial design of the digital wireframes are based on the findings and feedback of the user research. The wireframes would be used during the first usability test."
                lowfi_img={lowFi_img}>

                <PaperWireframesMain
                    pwireFrame_A={pwireFrame_A}
                    pwireFrame_B={pwireFrame_B}
                />

            </Prototype>

            <TestSection color="text-deepskyblue" iconColor="#00C9FF"
                         testInfo="I conducted a usability study to determine user needs when purchasing and reserving a movie ticket. During this process, I created a research plan which included KPI's, prompts, research goals, amongst others."
                         lengthInfo="5 - 10 minutes"
                         partInfo="5 participants" UsabilityInfo="This part of my project shows the problems participants had when using the app, and the changes I
                                                                       made based on their problems." firstImgheading="Log in option"
                         firstImginfo1="2 users addressed the fact that there is no login option when they are signing up in case they make a mistake of selecting the sign up option."
                         firstImginfo2="To solve this issue, I added the a login option at the bottom of the screen for easy access." firstImg={log_in_option}
                         secondImg={close_option}
                         secondImginfo1="3 users pointed out that they want the option of closing the pop-up that comes when reserving a ticketing in case they change their minds."
                         secondImginfo2="I solved this issue by adding a 'close' icon to the pop-up at the top right corner so it is visible to users."
                         secondImgheading="Close option"
            >
                <VisualMockups visual_mockup_info="After the tests and iterations, I brought my app to life by including visual elements; typography,
                                                                       icons, color, amongst others."
                               highFidelitymockup={high_fidelity}
                               testInfo1="For the second round of testing, I asked 5 participants to use the app to purchase and reserve a movie ticket. The participants were very excited about the app with one participant stating that it would be great if Silverbird Cinemas had an app like this. Fortunately, participants had few problems with the app. A few stated that there is no way to add a movie to their favorites and there is no indication of their seat number."
                               testInfo2="I dealt with the first issue by adding a love icon at the top right corner of a selected movie which users can select to add to their favorites. And to solve the last issue, I added a summary section of the row, seat number and the number of tickets by the 'buy' button on the showtime selection screen.
                          l also went further to add a 'cancel' button to the second pop-up when a user is reserving a ticket. This would allow the user to cancel their order instead of confirming it. All these changes can be seen in the image above."
                               prototypeButton={<PrototypeButton ptext="View Protoype"
                                                                 prototypelink="https://www.figma.com/proto/k2InuXouWdY9PnLzE7Bh20/Google-Project?page-id=155%3A4&node-id=155%3A5&viewport=324%2C-19%2C0.20972009003162384&scaling=scale-down"/>}
                />

                <NormalAccessibility
                    accessibilityConsiderations="To make the Silverbird Cinema app accessible, I made use of bold text and colors that would improve visibility. I also made use of colors, text and icons to indicate importance and bring users attention to where it needs to be. The use of speech recognition which is visible when users select the search icon is also incorporated to make the app user-friendly."
                />
            </TestSection>


            <FinalSection next_project_button={<Button text="Next Project" next_project="/forever-buddy"/>} takeaways={<Takeaways
                takeaway_info='This project is my fourth UI/UX project which I did as part of the Google UX Design Course. I had so much fun designing the app and others also enjoyed using the app. The app makes people want to go to the cinema more during their free time because it is now convenient. One quote from peer feedback: “I like the fact that the app allows you to watch trailers, view detailed movie information and check if tickets are available for a movie. My favorite part is being able to buy a ticket in advance or reserve it”'>
                <div className="text-gray-700 w-4/6 mx-auto mt-10 mb-6">During the course, I learned that designing for every single person is important. I also learned
                                                                        that usability studies are just important as user research because they all focus on one thing;
                                                                        user needs.
                </div>

            </Takeaways>}
                          nextsteps="Conduct another round of user research and usability studies since it would be one of the best way to update the app and meet user needs."/>

            <Contact/>


        </section>
    )
}

export default Silverbird;