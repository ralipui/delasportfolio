import React from "react";
import Topsection from "../CaseStudySections/Topsection";
import hero_image from "../../img/TradeCraft/Hero-Images.jpg";
import Nav from "../HomePage/Nav";
import Objectives from "../CaseStudySections/Objectives";
import AppConcept from "../CaseStudySections/AppConcept";
import AppConceptTradecraft from "../case-study-contents/AppConceptContent/AppConceptTradecraft";
import TargetAudience from "../CaseStudySections/TargetAudience";
import TradecraftAudience from "../case-study-contents/TargetAudienceContent/TradecraftAudience";
import Process from "../CaseStudySections/Process";
import Research from "../CaseStudySections/Research";
import AffinityTradecraft from "../case-study-contents/ImagesFiles/AffinityTradecraft";
import ResearchTradecraft from "../case-study-contents/Research-and-Define/ResearchTradecraft";
import Ideate from "../CaseStudySections/Ideate";
import ideate_img_1 from "../../img/TradeCraft/ideate_img_1.jpg";
import ideate_img_2 from "../../img/TradeCraft/ideate_img_2.jpg";
import Strategy from "../case-study-contents/DesignFramework/Strategy";
import Outline from "../case-study-contents/DesignFramework/Outline";
import outline_img from "../../img/TradeCraft/Outline_of_Scope.jpg";
import InfoArchitecture from "../case-study-contents/DesignFramework/InfoArchitecture";
import sitemap from "../../img/TradeCraft/TradeCraft-Sitemap.jpg";
import Design from "../case-study-contents/DesignFramework/Design";
import mood_board from "../../img/TradeCraft/Moodboard.jpg";
import logo_design from "../../img/TradeCraft/Logo.jpg";
import design_set from "../../img/TradeCraft/Design-Set.jpg";
import DesignFramework from "../CaseStudySections/DesignFramework";
import Prototype from "../CaseStudySections/Prototype";
import pwireFrame_A from "../../img/TradeCraft/pwireframe_A.jpg";
import pwireFrame_B from "../../img/TradeCraft/pwireframe_B.jpg";
import lowFi_img from "../../img/TradeCraft/low-fidelity.png"
import PaperWireframesMain from "../case-study-contents/Prototype/PaperWireframesMain";
import login from "../../img/TradeCraft/Login.png";
import contact from "../../img/TradeCraft/Contact.png";
import high_fidelity from "../../img/TradeCraft/visual_mockup.png";
import TestSection, {ThirdPart, VisualMockups, TwoGridAccessibility} from "../CaseStudySections/TestSection";
import work_documents from "../../img/TradeCraft/Work_documents.png";
import accessibility_img from "../../img/TradeCraft/Accessibility_Considerations.png";
import FinalSection, {Takeaways} from "../CaseStudySections/FinalSection";
import Contact from "../HomePage/Contact";
import Button, {PrototypeButton} from "../Features/Button";


function Tradecraft() {

    const listItems = ["To give homeowners or customers the ability to easily access handymen or home improvement companies.", "To help handymen and home improvement companies find more jobs.", "To provide users with home improvement ideas which they can implement to improve the state of their homes.", "To create a community of like-minded users to connect, share ideas and collaborate."];
    const user_needs = [
        "Login/Signup/Logout", "Use the app without having an account", "Create a personal or business account", "Receive notifications", "Explore home décor and improvement ideas including DIY ideas", "Communicate with other users", "Follow and unfollow other users", "Block or report other users", "Upload Resume", "Upload projects", "View contact info", "Post content", "Find and hire handymen or home improvement companies", "Make customizations in settings", "View information on business owners", "View category list and business owners list"];
    const client_needs = ["Increase TradeCraft members", "Promote businesses", "Increase users interests in home improvement", "Manually create accounts for handymen and home improvement companies", "Delete accounts of handymen and home improvement companies"];

    return (
        <section>
            <Nav/>
            <Topsection main_img={hero_image} title="Welcome to TradeCraft" description="TradeCraft is a mobile app that allows users in
                Ghana to have easy access to handymen such as plumbers, electricians, painters, amongst others, and home improvement
                companies that are nearby or live in the area." course="Course"
                        course_desc="UI/UX Specialization (CALARTS)" project="Project Duration"
                        duration="November 2020 - December 2020" role="My Role" the_role="UX Designer" tools="Tools"
                        the_tools="Adobe XD and Adobe Illustrator"
                        prototypeButtonA={<PrototypeButton ptext="View Protoype"
                                                           prototypelink="https://xd.adobe.com/view/a822ab95-7063-479f-b172-4af944dcef54-fd71/"/>}/>

            <Objectives objtitle="TradeCraft aims:"
                        problem="In the country I am from, Ghana, it is difficult to find such handymen and home improvement companies and that
            should not be the case because primarily the average person lives in a house. Once you live in a house, handymen are essentials and should not feel scarce when they are not. The feeling of scarcity is because of the difficulty in acquiring a handyman and since that is the situation, in turn, most handymen find it difficult to get jobs."
                        goals="Strategy an app that would allow users to have easy access to handymen and home improvement companies. And also allows handymen and home improvement companies find more jobs.">

                {listItems.map((listItem, index) => {
                    return <li key={index}>{listItem}</li>
                })}

            </Objectives>

            <AppConcept>
                <AppConceptTradecraft/>
                <hr/>
            </AppConcept>

            <TargetAudience color="text-maroon" gender="Both men and women"
                            education="College education or higher education"
                            occupation="Working class, college students"
                            age="18 and above"
                            location="Ghana, West Africa"
                            maritalStatus="More married people than single people"
                            income="All ranges"
                            personality="creative, curious, passionate, adventurous, problem solver, hardworking."
                            values="creativity, honesty, trust, open-mindedness, spontaneity, efficiency, quality work."
                            lifestyle="time and money conscious, family oriented, keeping up with the latest trends, trying out new things.">
                <TradecraftAudience/>
            </TargetAudience>

            <Process color="text-maroon" arrowColor="#5D053A"
                     research="User research, Personas, Journey maps and Competitive audit"
                     ideate="Crazy 8's and Storyboards"
                     framework="Strategy, Outline of scope, Information architecture and Strategy Process"
                     proto="Paper wireframes, Low fidelity wireframes, and prototype. Visual mockups and high fidelity prototype."
                     test="Usability test"
            />


            <Research>
                <ResearchTradecraft color="text-maroon" affinity={<AffinityTradecraft/>} research_item="To collect data, I interviewed 5 people, 3 buyers and 2 sellers. As stated earlier, the buyers and the sellers represent the types of users that would use the TradeCraft app.
                       I asked the participants questions concerning the services of handymen, companies and clients. And I also asked what they would expect in an app that solved their issue.
                       I created an affinity map of the responses that I received from the interviews and the surveys. The map allows me to organize my ideas in groups."
                />

            </Research>


            <Ideate color="text-maroon mb-6" crazy_img={ideate_img_1} story_img={ideate_img_2} storyboard="Big Picture Storyboard" story_content="I created a storyboard to show how Naa knew about the app, and how she used it to meet her needs which is to have easy access to qualified
                                handymen.">

                <div className="mb-2">Out of the 8 ideas, 3 stood out to me:</div>

                <div><span className="font-bold text-black">The third image:</span> A group chat where people can talk about anything home related.</div>

                <div><span className="font-bold text-black">The fifth image:</span> An online shop in the app that allows users to access home products.</div>

                <div><span className="font-bold text-black">The eight image:</span> Users can upload jobs/projects that handymen or home improvement companies
                                                                                    can apply for.
                </div>
            </Ideate>

            <DesignFramework>

                <Strategy color="text-maroon" user_needs_txt="The app should enable the user to:" client_needs_txt="The app should enable the client to:"
                          client_needs={client_needs.map((client_need, index) => {
                              return (
                                  <li key={index}>{client_need}</li>
                              )
                          })}
                          user_needs={user_needs.map((user_need, index) => {
                              return (
                                  <li key={index}>{user_need}</li>
                              )
                          })}
                />


                <Outline
                    outline_txt="The outline of scope consists of the content and functionality requirements. The content requirements show what the users expect to see on the app. And the functionality requirement shows what tasks the user can perform."
                    outline_img={outline_img}
                />
                <InfoArchitecture
                    arch_info="For my sitemap, I only focused on the sitemap for the buyers because of time. But the good news is that if I had done a sitemap for the sellers, it would look the same as the buyers, except for the signup and search processes. For the seller’s signup process, they would be required to enter their personal details, contact details and work details. The work details include work experience, portfolio, skills, etc. This can be seen in the wireframes and visual mockups. Below is a sitemap of TradeCraft which shows the structure of the app and how it will function."
                    sitemap={sitemap}
                />
                <Design
                    design_info="The design part of my project focuses on the UI design process of the app. The UI process provides a breakdown of the colors, font type, and images used to contribute to the look and feel of the app. This part of my work also includes the TradeCraft style guide."
                    mood_info="My mood board displays the logos, typography, color scheme, and images that inspired my work."
                    mood_img={mood_board}
                    logo_design_A={logo_design}
                    logo_process="I wanted a monogram logo for the TradeCraft app like the logos in my mood board, so, I focused on the initials TC, But I was not sure what it should look like. So, instead of using actual text, I decided to draw the T and C with the pen tool using Adobe Illustrator. I later thought of designing the T, I thought of the concept of the app and the handymen which inspired me to design the T as a nail, and I applied the same effect to the C. For the colors, I was inspired by the skyscrapers in my mood board, and I also did not want to go for the cliché blue."
                    style_guide={design_set}
                />

            </DesignFramework>

            <Prototype
                color="text-maroon"
                protoInfo="The image below shows initial sketches of a few screens from the app. I made these sketches when I was
                brainstorming on how the app would be designed."
                lowFi="The image below shows digital wireframes of the paper wireframes I sketched. The wireframes would
                be used during the first usability test."
                lowfi_img={lowFi_img}>

                <PaperWireframesMain
                    pwireFrame_A={pwireFrame_A}
                    pwireFrame_B={pwireFrame_B}
                />

            </Prototype>

            <TestSection color="text-maroon" iconColor="#5D053A"
                         testInfo="I conducted a usability study to determine user needs concerning the use of the app. During this process, I created a research plan which included KPI's, prompts, research goals, amongst others."
                         lengthInfo="5 - 10 minutes"
                         partInfo="4 participants"
                         UsabilityInfo="This part of my project shows the problems participants had when using the app, and the changes I made based on their problems."
                         firstImgheading="Other Log in/Sign up options"
                         firstImginfo1="3 users pointed out that they would want to log in or sign up using other accounts such as their google and facebook accounts. Using this approach, it would save the time of having to enter their personal details."
                         firstImg={login}
                         secondImg={contact}
                         secondImginfo1="2 users addressed the fact that there is not way to contact a hom e service using the TradeCraft platform. I corrected this by adding a link that would allow users to contact a home service using the TradeCraft platform."
                         secondImgheading="Contact with TradeCraft platform"

            >

                <ThirdPart thirdImgheading="Other forms of work documents"
                           thirdImginfo1="3 users were not happy about the fact that they could only upload a link or a document. So, I included the option of uploading images."
                           thirdImg={work_documents}/>

                <VisualMockups
                    visual_mockup_info="After the tests and iterations, I brought my app to life by including visual elements; typography, icons, color, amongst others."
                    highFidelitymockup={high_fidelity}
                    testInfo1="I asked 4 participants; 2 buyers and 2 sellers, to test the app on my mobile phone. I asked them to perform the following tasks: for buyers: create an account and search for a handyman. For sellers: create an account."
                    testInfo2="I can happily say that I mostly received positive feedback. Two participants stated that they liked the colors I used; it was refreshing. They all stated that the idea behind the app was a good idea since handymen are hard to find in Ghana. One guy pointed out that the signup process was a long process for him even though he understands why it is a long process."
                    prototypeButton={<PrototypeButton ptext="View Protoype"
                                                      prototypelink="https://xd.adobe.com/view/a822ab95-7063-479f-b172-4af944dcef54-fd71/"/>}
                />

                <TwoGridAccessibility
                    access_title="Accessibility Considerations"
                    accessibility="To make TradeCraft accessible for all, accessibility settings are included in settings, giving users access to a wide range of accessible features that include hearing enhancements and visibility enhancements. The app also works with the phone's accessibility settings  which makes it easier for the app to use the accessibility features that are active on the phone."
                    accessibility_img={accessibility_img}
                />

            </TestSection>

            <FinalSection next_project_button={<Button text="Next Project" next_project="/mymedic"/>} takeaways={<Takeaways
                takeaway_info="This was my first project since I started UI/UX design in 2020, I can honestly say it was not easy but it was fun. While I enjoyed working on this project, there are a few things that I learned along the way:">
                <div className="text-gray-700 text-left mt-16 mb-6"><span className="font-bold">There is no problem too small or too big.</span> A problem is a
                                                                                                                                                 problem depending
                                                                                                                                                 on
                                                                                                                                                 where you are. From
                                                                                                                                                 what I know about
                                                                                                                                                 other countries
                                                                                                                                                 like
                                                                                                                                                 the United States
                                                                                                                                                 or
                                                                                                                                                 Canada, apps like
                                                                                                                                                 my
                                                                                                                                                 app already exist
                                                                                                                                                 so it may not seem
                                                                                                                                                 like the TradeCraft
                                                                                                                                                 app is solving a
                                                                                                                                                 problem. But in
                                                                                                                                                 Ghana, it is a huge
                                                                                                                                                 problem. There are
                                                                                                                                                 quite a number of
                                                                                                                                                 problems that
                                                                                                                                                 should
                                                                                                                                                 not even exist in
                                                                                                                                                 Ghana and can be
                                                                                                                                                 solved with
                                                                                                                                                 technology. And I
                                                                                                                                                 am
                                                                                                                                                 very blessed to
                                                                                                                                                 know
                                                                                                                                                 that I can identify
                                                                                                                                                 a
                                                                                                                                                 few of these
                                                                                                                                                 problems
                                                                                                                                                 and try to solve
                                                                                                                                                 them
                                                                                                                                                 the best way I can.
                </div>

                <div className="text-gray-700 text-left mt-16 mb-6"><span className="font-bold">The user is very important.</span> This might seem very obvious but
                                                                                                                                   important, and for someone like me that
                                                                                                                                   just started my UI/UX journey, I learnt
                                                                                                                                   that the hard way. Just like Frank
                                                                                                                                   Chimero said, “People ignore designs
                                                                                                                                   that ignore people.” The users
                                                                                                                                   definitely come first when designing a
                                                                                                                                   product; it is about what would make
                                                                                                                                   them comfortable. They might not always
                                                                                                                                   know what they want, but they
                                                                                                                                   definitely know what they do not want.
                                                                                                                                   The whole point of solving a problem is
                                                                                                                                   to help others, so what is the point if
                                                                                                                                   they are not considered in the process?

                </div>

                <div className="text-gray-700 text-left mt-16 mb-6"><span className="font-bold">Do not rush the process.</span> Good things take time and I believe this
                                                                                                                                is very true. As much as there is more
                                                                                                                                that I can do for my project, I can say
                                                                                                                                that I made a lot of changes to my project
                                                                                                                                when I went through it. At the beginning,
                                                                                                                                I was rushing because I was so excited and
                                                                                                                                way in over my head, which caused me to
                                                                                                                                make a lot of mistakes. Looking back, I
                                                                                                                                see a huge difference and I can say I am
                                                                                                                                proud of myself because I see progress.
                </div>


            </Takeaways>}
                          nextsteps="Conduct more user research and usability studies with more participants, especially participants that are handy workers. I would also focus on the business aspect of the app which focuses on the users that want to be hired (sellers)."
            />

            <Contact/>


        </section>
    )
}

export default Tradecraft;