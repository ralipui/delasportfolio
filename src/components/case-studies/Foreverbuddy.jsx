import React from "react";
import Nav from "../HomePage/Nav";
import Topsection from "../CaseStudySections/Topsection";
import hero_img from "../../img/Forever Buddy/Hero-Images.jpg";
import Objectives from "../CaseStudySections/Objectives";
import AppConcept from "../CaseStudySections/AppConcept";
import AppConceptforeverbuddy from "../case-study-contents/AppConceptContent/AppConceptforeverbuddy";
import TargetAudience from "../CaseStudySections/TargetAudience";
import ForeverbuddyAudience from "../case-study-contents/TargetAudienceContent/ForeverbuddyAudience";
import Process from "../CaseStudySections/Process";
import Research from "../CaseStudySections/Research";
import ResearchForeverBuddy from "../case-study-contents/Research-and-Define/ResearchForeverBuddy";
import ideate_img_1 from "../../img/Forever Buddy/crazy_img.jpg";
import IdeateB from "../case-study-contents/IdeateContent/IdeateB";
import Strategy from "../case-study-contents/DesignFramework/Strategy";
import Outline from "../case-study-contents/DesignFramework/Outline";
import outline_img from "../../img/Forever Buddy/Outline_of_Scope.jpg";
import InfoArchitecture from "../case-study-contents/DesignFramework/InfoArchitecture";
import sitemap from "../../img/Forever Buddy/Sitemap.jpg";
import Design from "../case-study-contents/DesignFramework/Design";
import mood_board from "../../img/Forever Buddy/Moodboard.png";
import logo_design from "../../img/Forever Buddy/Logo-Process.jpg";
import style_guide from "../../img/Forever Buddy/Style_Guide.jpg";
import DesignFramework from "../CaseStudySections/DesignFramework";
import artboard_A from "../../img/Forever Buddy/Artboard_A.jpg";
import artboard_B from "../../img/Forever Buddy/Artboard_B.jpg";
import lowFi_img from "../../img/Forever Buddy/lowfi_img.png";
import Prototype from "../CaseStudySections/Prototype";
import PaperWireframes from "../case-study-contents/Prototype/PaperWireframes";
import add_to_cart from "../../img/Forever Buddy/Add_all_to_cart.png";
import check_availability from "../../img/Forever Buddy/Check_Availability.png";
import TestSection, {ThirdPart, VisualMockups, NormalAccessibility, AccImage} from "../CaseStudySections/TestSection";
import visual_mockup from "../../img/Forever Buddy/visual_mockup.png";
import student_account from "../../img/Forever Buddy/Student_account.png";
import buddy_img from '../../img/Forever Buddy/Buddy.png';
import acc_img from "../../img/Forever Buddy/Traversal_Order.png";
import FinalSection, {Takeaways} from "../CaseStudySections/FinalSection";
import Contact from "../HomePage/Contact";
import Button, {PrototypeButton} from "../Features/Button";

const Foreverbuddy = () => {

    const listItems = ["For users to have easy access to school and office supplies.", "To make shopping for school and office supplies easier for users.", "For users to have easy access to a wide range of school and office supplies, especially supplies that are not available in the country.", "To provide affordable and quality products."];
    const user_needs = ["Login/Logout/Signup", "Create an account", "Use filters for searching", "Receive assistance from ‘Buddy’", "Order for products", "Pay online", "Search for a product using filters", "Find out the modes of payment", "View their past orders", "View product information", "Make customizations in settings."];
    const client_needs = ["Manually add and delete product and product information.", "Manually edit product information.", "Manually add, delete and edit product categories.", "Increase orders.", "Communicate reliability and efficiency."];

    return (
        <section>
            <Nav/>
            <Topsection main_img={hero_img} title="Welcome to Forever Buddy"
                        description="Forever Buddy is an online marketplace for school and office supplies."
                        course="Client"
                        course_desc="Forever Buddy (Personal Project)" project="Project Duration"
                        duration="March 2021 - April 2021" role="My Role" the_role="UX Designer" tools="Tools"
                        the_tools="Figma and Adobe Illustrator"
                        prototypeButtonA={<PrototypeButton ptext="View Protoype"
                                                           prototypelink="https://www.figma.com/proto/HAnvBs3nmFJzxd7jrv0CSk/Forever-Buddy?page-id=0%3A1&node-id=14%3A3&viewport=329%2C462%2C0.03&scaling=scale-down-width&starting-point-node-id=14%3A3"/>}/>

            <Objectives objtitle="Forever Buddy aims:"
                        problem="Concerning my personal experiences, when I was attending school in Ghana, my mom and I always struggled to get me supplies for school.
                        Things at the malls were expensive and we could not find everything in the market where things were cheaper. We also tried using online stores such as Jumia,
                        but the problem was that some of the things we needed were not in Ghana; they had to be imported. I guess this explains why a lot of Ghanaian students that have the opportunity to travel during the holidays prefer to buy their things from abroad.
                        Ghana definitely lacks a lot of resources that can improve education and make learning easier, and even if they do have them, they are very expensive."
                        goals="The goal is to provide students and workers with easy access to a wide range of affordable school and office supplies.">

                {listItems.map((listItem, index) => {
                    return <li key={index}>{listItem}</li>
                })}
            </Objectives>

            <AppConcept>
                <AppConceptforeverbuddy/>
                <hr/>
            </AppConcept>

            <TargetAudience color="text-darkcyan" gender="Both men and women"
                            education="Middle school, high school, college or higher education"
                            occupation="Working class, students"
                            age="14 - 35 years old"
                            location="Ghana, West Africa"
                            maritalStatus="Both single and married people"
                            income="All ranges"
                            personality="hardworking, youthful, independent, open-minded, responsible"
                            values="efficiency, determination, optimism, compassion"
                            lifestyle="studious, engaging in extracurricular activities, having part-time jobs.">
                <ForeverbuddyAudience/>
            </TargetAudience>

            <Process color="text-darkcyan" arrowColor="#00A99D"
                     research="User research, Personas, Journey maps and Competitive audit"
                     ideate="Crazy 8's and How Might We"
                     framework="Strategy, Outline of scope, Information architecture and Strategy Process"
                     proto="Paper wireframes, Low fidelity wireframes, and prototype. Visual mockups and high fidelity prototype."
                     test="Usability test"
            />

            <Research>
                <ResearchForeverBuddy color="text-darkcyan"
                                      research="For this part of my project, I conducted interviews to know what users need based on their experiences with other e-commerce websites. I interviewed 4 people; 2 teenagers between the ages and 16 and 19, and 2 adults between the ages of 28 and 35. My choice of age was deliberate because I wanted to understand the user needs from the perspective of students, and from the perspective of working adults and parents.
                                    After completing the interview, I grouped the responses of the 4 interviewees using an empathy map to identify what users feel, think, hear, see, say and do about their e-commerce experiences. Below is an empathy map of the 4 interviewees."/>

            </Research>

            <IdeateB color="text-darkcyan mb-6" rect_color="bg-darkcyan" crazy_img={ideate_img_1}
                     rectangle1="make the search process easier and faster for users?"
                     rectangle2="make Buddy accessible?" rectangle3="make Buddy more helpful?"
                     rectangle3_content="Allow users to communicate with Buddy using a chatbot."
                     rectangle2_content="Allow Buddy to communicate through speech for vision impaired users."
                     rectangle1_content="Implement the auto complete feature that allows  the computer to predict the rest of the words the user is typing.">

                <div className="mb-2">Out of the 8 ideas, 2 stood out to me:</div>

                <div><span className="font-bold text-black">The first image:</span> Users can change the Forever Buddy mascot to their favorite animal.</div>

                <div><span className="font-bold text-black">The seventh image:</span> Buddy provides recommends based on a user's search history on Google or any other
                                                                                      search engine they use.
                </div>

            </IdeateB>

            <DesignFramework>

                <Strategy color="text-darkcyan" user_needs_txt="The website needs to enable the user to:" client_needs_txt="The website should enable the client to:"
                          user_needs={user_needs.map((user_need, index) => {
                              return (
                                  <li key={index}>{user_need}</li>
                              )
                          })}
                          client_needs={client_needs.map((client_need, index) => {
                              return (
                                  <li key={index}>{client_need}</li>
                              )
                          })}

                />


                <Outline
                    outline_txt="The outline of scope consists of the content and functionality requirements. The content requirements show what the users expect to see on the website. And the functionality requirement shows what tasks the user can perform."
                    outline_img={outline_img}
                />
                <InfoArchitecture
                    arch_info="The information architecture of my project includes my sitemap and user flow. This part of my work would serve as the foundation for my wireframes."
                    sitemap={sitemap}
                />
                <div className="mt-16 px-20">
                    <div className="font-bold text-xl mb-2">User Flows</div>
                    <div className="mb-16 text-xl text-gray-700">Order Flows</div>
                    <img src={artboard_A} alt=""/>
                </div>
                <div className="mt-16 px-20">
                    <div className="mb-12 text-xl text-gray-700">Checkout Process</div>
                    <img src={artboard_B} alt=""/>
                </div>
                <Design
                    design_info="This part of my project focuses on the look and feel of my website. It includes the logo design process, my mood board, and the Forever Buddy style guide and UI kit."
                    mood_info="My mood board displays the logos, typography, color scheme, and images that inspired my work."
                    mood_img={mood_board}
                    logo_design_A={logo_design}
                    logo_process="During the logo design process, I was focused on designing a combination mark logo which consists of a wordmark and a mascot. The whole point of the mascot is to represent ‘Buddy’ who serves as helpmate or in this case, a shop mate for the user. After identifying the official color for the website which is Faux-Persian green, I decided that the ‘Buddy’ in Forever Buddy would be emphasized with the color and an informal font type. After finally getting the design I wanted, I was torn between two designs; having the mascot in place of the ‘u’ in Buddy, or placing the mascot on top of the Buddy. This can be seen in the 7th and 8th design in the image below. I finally settled on the 8th designs because I feared people would mistake the Buddy mascot for an ‘o’ or an ‘a’. But I am very happy with the final design especially because I have achieved what I wanted."
                    style_guide={style_guide}
                />

            </DesignFramework>

            <Prototype
                color="text-darkcyan"
                protoInfo="The images below show a sketch of the homepage, products, single product, cart, signup and one of the checkout pages. I did this when I started thinking about the structure of my website."
                lowFi="The image below shows digital wireframes of the paper wireframes I sketched. But the digital wireframes also include the other pages of the website. The wireframes would be used during the first usability test."
                lowfi_img={lowFi_img}>

                <PaperWireframes/>

            </Prototype>

            <TestSection color="text-darkcyan" iconColor="#00A99D"
                         testInfo="I conducted a usability study to determine user needs when using the website to order for a product, and also they use Buddy to search for a product. During this process, I created a research plan which included KPI's, prompts, research goals, amongst others."
                         lengthInfo="10 - 15 minutes"
                         partInfo="4 participants"
                         UsabilityInfo="This part of my project shows the problems participants had when ordering for a product and using Buddy to search for a product, and the changes I made based on their problems."
                         firstImgheading='"Add all to cart" option'
                         firstImginfo1="2 users mentioned that they would want a way to add all the products provided by Buddy to the cart instead of adding the products one at a time."
                         firstImginfo2='To solve this, I added a " Add All To Cart" button which allows users to add all the products recommended by Buddy.'
                         firstImg={add_to_cart}
                         secondImg={check_availability}
                         secondImginfo1="2 users pointed out that they would want to check the availability of the products and the number available before adding a product to their cart."
                         secondImginfo2='To solve the problem, I added a " Check
                         Availability" button which would allow users to check the availability of the products and the number available in the stores or warehouses.'
                         secondImgheading="Check Availability"

            >

                <ThirdPart thirdImgheading="Student account"
                           thirdImginfo1="2 users mentioned the option of creating a student account which would allow for student discounts and other student benefits."
                           thirdImginfo2="To solve the problem, I provided the option of creating a student account or a standard account when signing up."
                           thirdImg={student_account}/>

                <VisualMockups visual_mockup_info="After the tests and iterations, I brought my app to life by including visual elements; typography,
                                                                       icons, color, amongst others." highFidelitymockup={visual_mockup} testInfo1="After finishing the prototype, I got 4 participants to try out the prototype by performing the following tasks:
                                • Order for the blue printed backpack and go through the checkout process.
                                • Receive assistance from Buddy by telling Buddy what you need."
                               testInfo2="The feedback received from the participants was mostly positive. Participants mentioned that the ordering process was smooth and easy to understand, the colors used were creative, and the idea behind Buddy was also creative and helpful.
                          The only problem mentioned was the fact that the forms were too large, but overall, it was good. Even though I mostly received positive feedback, I plan on making changes to the forms and adding a shadow to the search bar, which I later observed to look flat."
                               prototypeButton={<PrototypeButton ptext="View Protoype"
                                                                 prototypelink="https://www.figma.com/proto/HAnvBs3nmFJzxd7jrv0CSk/Forever-Buddy?page-id=0%3A1&node-id=14%3A3&viewport=329%2C462%2C0.03&scaling=scale-down-width&starting-point-node-id=14%3A3"/>}/>

                <NormalAccessibility
                    accessibilityConsiderations="To make the Forever Buddy website accessible, I would make Buddy accessible. Buddy would be able to communicate through speech for users with impaired vision. And captions would be provided anytime Buddy communicates with the user."
                >
                    <div className="flex justify-center mt-8">
                        <img src={buddy_img} className="w-1/2" alt=""/>
                    </div>
                    <div className="flex justify-center">
                        <p className="mt-10 w-4/6 text-gray-700">Also, users would be able to use the keyboard to navigate the pages of the website. The image
                                                                 below shows
                                                                 annotations that
                                                                 indicate the order
                                                                 in which they would be able navigate the homepage. </p>

                    </div>
                </NormalAccessibility>
                <AccImage
                    acc_img={acc_img}
                />

            </TestSection>

            <FinalSection next_project_button={<Button text="Next Project" next_project="/noodles-city"/>} takeaways={<Takeaways
                takeaway_info="This project is my third UI/UX project, and my first e-commerce website design. To be honest, it was not easy, especially because I do not do online shopping so I had to do a lot of research to know what each page would look like, especially the checkout process. But at the end of it all, I was very proud of myself because I saw the improvement in my design skills. I compared the order process to that of my Noodles City project and noticed the things I can change in the project based on what I learned from the Forever Buddy project. One thing I learned from doing this project is that I should learn to take risks. Some risks are worth taking and that is what I experienced with doing the Forever Buddy project. I was very nervous to start the project and felt that doing this project would define my skills as a designer. But I decided to focus on the positive side which was to improve my skills and become good at what I do. I am not saying my work is perfect, but I see the improvement in my design skills and I can confidently say that this project was worth it."/>}
                          nextsteps="Conduct more user research, usability studies and make a few changes to the checkout process of the website. I will figure out ways in which Buddy can be more accessible if possible."
            />

            <Contact/>


        </section>
    )

}

export default Foreverbuddy;