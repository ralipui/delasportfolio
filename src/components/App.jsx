import React from "react";
import {Route, Switch} from "react-router-dom";
import HomePage from "./Homepage";
import Tradecraft from "./case-studies/Tradecraft";
import Mymedic from "./case-studies/Mymedic";
import Silverbird from "./case-studies/Silverbird";
import Foreverbuddy from "./case-studies/Foreverbuddy";
import Noodlescity from "./case-studies/Noodlescity";


function App() {
    return (
        <div>
            <Switch>
                <Route path="/tradecraft">
                    <Tradecraft/>
                </Route>
                <Route path="/mymedic">
                    <Mymedic/>
                </Route>
                <Route path="/silverbird">
                    <Silverbird/>
                </Route>
                <Route path="/forever-buddy">

                    <Foreverbuddy/>
                </Route>
                <Route path="/noodles-city">
                    <Noodlescity/>
                </Route>
                <Route path="/">
                    <div className="overflow-y-hidden">
                        <HomePage/>
                    </div>
                </Route>
            </Switch>

        </div>
    )
}


export default App;