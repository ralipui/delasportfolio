import React from "react";
import Title from "../Features/Title";
import TwoGrid from "../Features/TwoGrid";


const Ideate = (props) => {

    return (
        <section className="mt-16">
            <div className="px-20 font-montserrat text-center">
                <Title className={props.color}>Ideate</Title>
                <TwoGrid>
                    <div className="w-5/6">
                        <img src={props.crazy_img} alt=""/>

                    </div>
                    <div>
                        <div className="font-bold mb-7">Crazy 8's</div>
                        <div className="text-gray-700">
                            <div className="mb-8 leading-relaxed">
                                As part of my ideation process, I did the Crazy 8's exercise. The purpose of the Crazy 8's exercise is to help generate a wide variety of
                                innovative solutions in eight minutes.
                            </div>
                            {props.children}

                        </div>
                    </div>
                </TwoGrid>

                <TwoGrid className="mt-16">
                    <div className="w-5/6">
                        <div className="font-bold mb-7">{props.storyboard}</div>
                        <div className="text-gray-700">
                            <div className="mb-8 leading-relaxed">
                                {props.story_content}
                            </div>
                        </div>
                    </div>

                    <div className="">
                        <img src={props.story_img} alt=""/>

                    </div>
                </TwoGrid>

            </div>
            <hr/>
        </section>
    )
}

export default Ideate;