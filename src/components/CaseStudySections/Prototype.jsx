import React from "react";
import Title from "../Features/Title";


const Prototype = (props) => {

    return (
        <section className="mt-16">
            <div className="px-20 font-montserrat text-center mb-16">
                <Title className={props.color}>Prototype</Title>
                <div className="font-bold text-xl mb-10">Paper Wireframes</div>
                <div className="text-gray-700 mb-12 w-4/6 mx-auto">{props.protoInfo}<span className="font-bold italic">{props.boldProtoInfo}</span></div>
                {props.children}
                <div className="mt-16 px-20">
                    <div className="font-bold mb-12 text-xl">Low-Fidelity Wireframes</div>
                    <div className="text-gray-700 mb-12 w-5/6 mx-auto">{props.lowFi}
                    </div>
                    <img src={props.lowfi_img} alt=""/>
                </div>

            </div>
            <hr/>
        </section>
    )
}

export default Prototype;