import React from "react";
import TwoGrid from "../Features/TwoGrid";

const Objectives = (props) => {

    return (
        <section className="mt-16">
            <div className="px-20 font-montserrat">
                <TwoGrid>
                    <div className="pr-10">
                        <div className="font-bold mb-7">Objectives</div>
                        <div className="max-w-xl">
                            {props.objtitle}
                            <ol className="list-decimal pl-8 leading-7 text-gray-700">
                                {props.children}
                            </ol>
                        </div>
                    </div>

                    <div>
                        <div className="font-bold mb-7">The Problem</div>
                        <div className="leading-7 text-gray-700">
                            {props.problem}
                        </div>
                    </div>
                </TwoGrid>

                <div className="flex-col text-center max-w-3xl mx-auto py-12">
                    <div className="font-bold mb-7">The Goal</div>
                    <div className="leading-7 text-gray-700">{props.goals}
                    </div>

                </div>
            </div>
            <hr/>
        </section>
    )
}

export default Objectives;