import React from "react";

const TopSection = (props) => {

    return (
        <section>
            <div className="w-full pt-16">
                <img src={props.main_img} alt=""/>
                <div className="px-20 text-center mt-20">
                    {props.prototypeButtonA}

                </div>
            </div>


            <div className="px-20 pt-32 font-montserrat flex-col text-center space-y-10">


                <div className="font-bold text-2xl">{props.title}</div>
                <div className="max-w-4xl mx-auto text-md text-gray-700">{props.description}
                </div>
                <div className="grid grid-cols-4 px-20">
                    <div className="p-8">
                        <div className="font-bold">{props.course}</div>
                        <div className="text-gray-700">{props.course_desc}</div>
                    </div>
                    <div className="p-8">
                        <div className="font-bold">{props.project}</div>
                        <div className="text-gray-700">{props.duration}</div>
                    </div>
                    <div className="p-8">
                        <div className="font-bold">{props.role}</div>
                        <div className="text-gray-700">{props.the_role}</div>
                    </div>
                    <div className="p-8">
                        <div className="font-bold">{props.tools}</div>
                        <div className="text-gray-700">{props.the_tools}</div>
                    </div>
                </div>
            </div>
            <hr/>

        </section>
    )
}

export default TopSection;