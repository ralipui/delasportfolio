import React from "react";


const FinalSection = (props) => {
    return (
        <section className="mt-16">
            <div className="px-20 font-montserrat text-center mb-16">
                <div className="flex flex-col">
                    {props.takeaways}

                    <div className="font-bold text-xl mt-16">Next Steps</div>
                    <div className="text-gray-700 w-4/6 mx-auto mt-10">{props.nextsteps}</div>
                    <div className="font-bold text-4xl mt-32 mb-32">Thank you for reading!</div>

                </div>

                {props.next_project_button}
            </div>
            {props.children}
            <hr/>
        </section>
    )
}

const Takeaways = (props) => {

    return (
        <section className="mt-20">
            <div className="font-bold text-xl">Takeaways</div>
            <div className="text-gray-700 w-4/6 mx-auto mt-10 mb-6">{props.takeaway_info}</div>
            {props.children}
        </section>
    )
}

export default FinalSection;
export {Takeaways}