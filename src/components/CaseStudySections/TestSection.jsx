import React from "react";
import Title from "../Features/Title";
import TestIcon from "../Features/TestIcon";
import TwoGrid from "../Features/TwoGrid";

const TestSection = (props) => {

    return (

        <section className="mt-16">
            <div className="px-20 font-montserrat text-center mb-16">
                <Title className={props.color}>Test</Title>
                <div className="text-gray-700 w-4/6 mx-auto mb-12">{props.testInfo}</div>

                <div className="flex flex-col mt-12">
                    <TestIcon color={props.iconColor}/>
                    <div className="flex text-gray-700 mt-6 justify-center">
                        <div className="w-1/6 flex flex-col">
                            <span className="font-bold">Study Type: </span>
                            <span>Unmoderated</span>
                            <span>Usability Study</span>
                        </div>
                        <div className="w-1/6 flex flex-col">
                            <span className="font-bold">Length:</span>
                            <span>{props.lengthInfo}</span>
                        </div>
                        <div className="w-1/6 flex flex-col">
                            <span className="font-bold">Participants:</span>
                            <span>{props.partInfo}</span>
                        </div>
                        <div className="w-1/6 flex flex-col">
                            <span className="font-bold">Location:</span>
                            <span>Ghana; remote</span>
                        </div>

                    </div>
                </div>


                <div className="mt-16 pt-16">
                    <div className="font-bold mb-8 text-md">Usability Study Insights and Iterations</div>
                    <div className="text-gray-700 mb-12 w-3/6 mx-auto">{props.UsabilityInfo}</div>

                    <TwoGrid className="pt-16 mb-16">
                        <div className="w-5/6 my-auto">
                            <div className="font-bold mb-7">{props.firstImgheading}</div>
                            <div className="text-gray-700">
                                <div className="mb-8 leading-relaxed">{props.firstImginfo1}
                                </div>
                                <div className="mb-8 leading-relaxed">{props.firstImginfo2}
                                </div>
                            </div>
                        </div>

                        <div className="">
                            <img src={props.firstImg} className="w-5/6 mx-auto" alt=""/>

                        </div>
                    </TwoGrid>

                    <TwoGrid className="pt-16 mb-16">
                        <div className="">
                            <img src={props.secondImg} className="w-5/6 mx-auto" alt=""/>

                        </div>

                        <div className="w-5/6 my-auto">
                            <div className="font-bold mb-7">{props.secondImgheading}</div>
                            <div className="text-gray-700">
                                <div className="mb-8 leading-relaxed">{props.secondImginfo1}</div>
                                <div className="mb-8 leading-relaxed">{props.secondImginfo2}</div>
                            </div>
                        </div>
                    </TwoGrid>
                </div>


                {props.children}


            </div>
            <hr/>
        </section>

    )
}


const ThirdPart = (props) => {

    return (
        <TwoGrid className="pt-16 mb-16">

            <div className="w-5/6 my-auto">
                <div className="font-bold mb-7">{props.thirdImgheading}</div>
                <div className="text-gray-700">
                    <div className="mb-8 leading-relaxed">{props.thirdImginfo1}</div>
                    <div className="mb-8 leading-relaxed">{props.thirdImginfo2}</div>
                </div>
            </div>

            <div className="">
                <img src={props.thirdImg} className="w-5/6 mx-auto" alt=""/>

            </div>
        </TwoGrid>

    )

}


const FourthPart = (props) => {
    return (

        <TwoGrid className="pt-16 mb-16">

            <div className="">
                <img src={props.fourthImg} className="w-5/6 mx-auto" alt=""/>

            </div>

            <div className="w-5/6 my-auto">
                <div className="font-bold mb-7">{props.fourthImgheading}</div>
                <div className="text-gray-700">
                    <div className="mb-8 leading-relaxed">{props.fourthImginfo1}</div>
                    <div className="mb-8 leading-relaxed">{props.fourthImginfo2}</div>
                </div>
            </div>
        </TwoGrid>

    )
}

const VisualMockups = (props) => {

    return (
        <section className="mb-20">
            <div className="mt-16 pt-16">
                <div className="font-bold mb-8 text-md">Visual Mockups and High-Fidelity Prototype</div>
                <div className="text-gray-700 mb-12 w-3/6 mx-auto">{props.visual_mockup_info}
                </div>
                <div className="flex justify-center">
                    <img src={props.highFidelitymockup} alt="high fidelity prototype"/>

                </div>
            </div>

            <div className="mt-16 pt-16">
                <div className="font-bold mb-8 text-lg">Test</div>
                <div className="text-gray-700 mb-12 w-4/6 mx-auto">{props.testInfo1}</div>
                <div className="text-gray-700 mb-12 w-4/6 mx-auto">{props.testInfo2}</div>

                {props.prototypeButton}

            </div>
        </section>


    )
}

const NormalAccessibility = (props) => {

    return (
        <div className="mt-16 pt-16">
            <div className="font-bold mb-8 text-lg">Accessibility Considerations</div>
            <div className="text-gray-700 w-4/6 mx-auto">{props.accessibilityConsiderations}</div>
            {props.children}
        </div>


    )
}


const TwoGridAccessibility = (props) => {

    return (
        <TwoGrid className="pt-24 mb-16">
            <div className="w-5/6 my-auto">
                <div className="font-bold mb-7">{props.access_title}</div>
                <div className="text-gray-700">
                    <div className="mb-8 leading-relaxed">{props.accessibility}</div>
                </div>
            </div>

            <div className="">
                <img src={props.accessibility_img} className="w-1/2 mx-auto" alt=""/>

            </div>

        </TwoGrid>
    )

}

const AccImage = (props) => {
    return (
        <div>
            <img src={props.acc_img} alt=""/>
        </div>

    )
}


// const ForeverBuddyImage = () => {
//
//     return (
//         <div>
//             <img src="" alt=""/>
//
//         </div>
//     )
// }


export default TestSection;
export {ThirdPart, FourthPart, VisualMockups, NormalAccessibility, TwoGridAccessibility, AccImage}