import React from "react";


const DesignFramework = (props) => {

    return (

        <section className="mt-16 pb-10">
            <div className="px-20 font-montserrat text-center">

                {props.children}
            </div>
            <hr/>
        </section>
    )

}

export default DesignFramework;