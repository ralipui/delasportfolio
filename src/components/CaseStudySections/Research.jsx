import React from "react";


const Research = (props) => {
    return (

        <section className="mt-16">
            {props.children}
            <hr/>
        </section>
    )

}

export default Research;