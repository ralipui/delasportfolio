import React from "react";


const AppConcept = (props) => {


    return (
        <section className="mt-16">
            {props.children}
        </section>
    )
}

export default AppConcept;