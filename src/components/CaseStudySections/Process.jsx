import React from "react";
import Title from "../Features/Title";
import Arrow from "../Features/Arrow";

const Process = (props) => {

    return (
        <section className="mt-16">
            <div className="px-20 font-montserrat flex flex-col text-center space-y-10 pb-16">
                <Title className={props.color}>My Process</Title>
                <Arrow color={props.arrowColor}/>
                <div className="grid grid-cols-5">
                    <div className="flex flex-col w-3/5 mx-auto">
                        <div className="font-bold mb-2">Research & Define</div>
                        <div className="text-gray-700">{props.research}
                        </div>

                    </div>
                    <div className="flex flex-col w-5/6 mx-auto">
                        <div className="font-bold mb-2">Ideate</div>
                        <div className="text-gray-700">
                            {props.ideate}
                        </div>
                    </div>
                    <div className="flex flex-col w-4/6 mx-auto">
                        <div className="font-bold mb-2">Design Framework</div>
                        <div className="text-gray-700">{props.framework}
                        </div>

                    </div>
                    <div className="flex flex-col w-3/5 mx-auto">
                        <div className="font-bold mb-2">Prototype</div>
                        <div className="text-gray-700">{props.proto}
                        </div>

                    </div>
                    <div className="flex flex-col w-5/6 mx-auto">
                        <div className="font-bold mb-2">Ideate</div>
                        <div className="text-gray-700">Crazy 8's
                                                       How Might We
                        </div>


                    </div>
                </div>
            </div>
            <hr/>
        </section>
    )

}

export default Process;