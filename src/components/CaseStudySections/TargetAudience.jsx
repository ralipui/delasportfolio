import React from "react";
import Title from "../Features/Title";


const TargetAudience = (props) => {

    return (
        <section className="mt-16">
            <div className="px-20 font-montserrat flex flex-col text-center space-y-10 pb-16">
                <Title className={props.color}>Target Audience</Title>
                <div className="font-bold mb-14 text-xl">Roles</div>
                {props.children}
                <div className=" flex flex-col pt-10 pb-20">
                    <div className="font-bold mb-14 text-xl">Demographics</div>
                    <div className="grid grid-cols-4 px-20 pb-8">
                        <div>
                            <div className="font-bold mb-2">Gender</div>
                            <div className="text-gray-700">{props.gender}</div>
                        </div>

                        <div>
                            <div className="font-bold mb-2">Education</div>
                            <div className="text-gray-700">{props.education}</div>
                        </div>

                        <div>
                            <div className="font-bold mb-2">Occupation</div>
                            <div className="text-gray-700">{props.occupation}</div>
                        </div>

                        <div>
                            <div className="font-bold mb-2">Age</div>
                            <div className="text-gray-700">{props.age}</div>
                        </div>
                    </div>

                    <div className="grid grid-cols-3 px-28">
                        <div>
                            <div className="font-bold mb-2">Location</div>
                            <div className="text-gray-700">{props.location}</div>
                        </div>

                        <div>
                            <div className="font-bold mb-2">Marital Status</div>
                            <div className="text-gray-700">{props.maritalStatus}</div>
                        </div>

                        <div>
                            <div className="font-bold mb-2">Income</div>
                            <div className="text-gray-700">{props.income}</div>
                        </div>
                    </div>
                </div>

                <div className="flex flex-col">
                    <div className="font-bold mb-12 text-xl">Psychographics</div>
                    <div className="grid grid-cols-3 max-w-5xl mx-auto gap-x-10">
                        <div>
                            <div className="font-bold mb-2">Personality & Attitudes</div>
                            <div className="text-gray-700">{props.personality}
                            </div>
                        </div>
                        <div>
                            <div className="font-bold mb-2">Values</div>
                            <div className="text-gray-700">{props.values}
                            </div>
                        </div>
                        <div>
                            <div className="font-bold mb-2">Lifestyles</div>
                            <div className="text-gray-700">{props.lifestyle}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
        </section>
    )
}

export default TargetAudience;