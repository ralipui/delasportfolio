import React from "react";
import Nav from "./HomePage/Nav";
import Home, {About, NavBottom, YearsExperience} from "./HomePage/Home";
import Skills from "./HomePage/Skills";
import Works from "./HomePage/Works";
import Footer from "./HomePage/footer";
import Contact, {NewFooter} from "./HomePage/Contact";

const HomePage = () => {
    return (
        <section className="h-screen">
            <Nav/>
            {/*<Home/>*/}
            {/*<About/>*/}
            <Skills/>
            {/*<YearsExperience/>*/}
            <NavBottom/>
            {/*<Works/>*/}
            <div className="flex absolute w-full bottom-0">
                <NewFooter/>
            </div>


        </section>

    )
}

export default HomePage;