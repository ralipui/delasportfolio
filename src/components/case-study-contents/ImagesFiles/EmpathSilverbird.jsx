import React from "react";
import empathy_map from "../../../img/Silverbird/Empathy Map.jpg";


const EmpathSilverbird = () => {
    return <img src={empathy_map} alt=""/>

}

export default EmpathSilverbird;