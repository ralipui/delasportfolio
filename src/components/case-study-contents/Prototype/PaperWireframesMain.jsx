import React from "react";
import TwoGrid from "../../Features/TwoGrid";


const PaperWireframesMain = (props) => {

    return (
        <TwoGrid className="mt-16">
            <div className="">
                <img src={props.pwireFrame_A} alt=""/>

            </div>
            <div className="">
                <img src={props.pwireFrame_B} alt=""/>

            </div>
        </TwoGrid>
    )
}

export default PaperWireframesMain;