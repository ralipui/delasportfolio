import React from "react";
import pwireframe_A from "../../../img/Forever Buddy/pwireframe_A.jpg"
import pwireframe_B from "../../../img/Forever Buddy/pwireframe_B.jpg"
import pwireframe_C from "../../../img/Forever Buddy/pwireframe_C.jpg"

const PaperWireframes = () => {

    return (
        <div className="mt-16">
            <div className="flex space-x-10">
                <div><img src={pwireframe_A} alt=""/></div>
                <div><img src={pwireframe_B} alt=""/></div>
                <div><img src={pwireframe_C} alt=""/></div>
                
            </div>
        </div>
    )
}

export default PaperWireframes;