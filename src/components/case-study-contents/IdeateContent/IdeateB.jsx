import React from "react";
import TwoGrid from "../../Features/TwoGrid";
import Title from "../../Features/Title";
import Rectangle from "../../Features/Rectangle";


const IdeateB = (props) => {

    const rect_color_1_and_2 = props.rect_color + " px-2";
    const rect_color_3 = props.rect_color + " px-3";


    return (

        <section className="mt-16">
            <div className="px-20 font-montserrat text-center">
                <Title className={props.color}>Ideate</Title>
                <TwoGrid>
                    <div className="w-5/6">
                        <img src={props.crazy_img} alt=""/>

                    </div>
                    <div>
                        <div className="font-bold mb-7">Crazy 8's</div>
                        <div className="text-gray-700">
                            <div className="mb-8 leading-relaxed">
                                As part of my ideation process, I did the Crazy 8's exercise. The purpose of the Crazy 8's exercise is to help generate a wide variety of
                                innovative solutions in eight minutes.
                            </div>
                            {props.children}

                        </div>
                    </div>
                </TwoGrid>

                <div className="mt-16 mb-8">
                    <div className="font-bold mb-7">"How might We"</div>
                    <div className="text-gray-700 w-2/3 mx-auto mb-16">I used the "how might we" design thinking technique to help think about problems from different
                                                                       perspectives, making it easier to come up with a
                                                                       wide variety of creative solutions.
                    </div>


                    <div className="flex justify-between px-32">
                        <Rectangle className={rect_color_1_and_2}>
                            <span className="font-bold">How might we</span> {props.rectangle1}
                        </Rectangle>
                        <Rectangle className={rect_color_1_and_2}>
                            <span className="font-bold">How might we</span> {props.rectangle2}
                        </Rectangle>
                        <Rectangle className={rect_color_3}>
                            <span className="font-bold">How might we</span> {props.rectangle3}
                        </Rectangle>
                    </div>

                    <div className="flex justify-evenly mt-10 text-gray-700">
                        <div className="w-1/4">
                            {props.rectangle1_content}
                        </div>
                        <div className="w-1/4">
                            {props.rectangle2_content}
                        </div>
                        <div className="w-1/4">
                            {props.rectangle3_content}
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
        </section>
    )
}

export default IdeateB;