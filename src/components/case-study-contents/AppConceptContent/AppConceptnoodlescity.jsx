import React from "react";
import Title from "../../Features/Title";
import TwoGrid from "../../Features/TwoGrid";

const AppConceptnoodlescity = () => {

    const noodles = ["Instant noodles", "Fried noodles", "Thai noodles", "Japanese pan noodles"];
    const proteins = ["Chicken", "Fish", "Goat", "Sausage", "Pork", "Beef", "Prawns", "Eggs", "Shrimps", "Squid"];
    const vegetables = ["Tomatoes", "Carrots", "Cucumber", "Onions", "Green pepper", "Cabbage"];
    const sauces = ["Garlic sauce", "Soy sauce", "Chili sauce"];


    return (
        <div className="px-20 font-montserrat flex flex-col text-center space-y-10">
            <Title className="text-tomato">Restaurant Concept</Title>
            <div className="grid grid-cols-4 px-20 pb-8 gap-y-14">
                <div>
                    <div className="font-bold mb-2">Restaurant Name & URL</div>
                    <div className="flex flex-col text-gray-700">
                        <span>Noodles City</span>
                        <span>noodlescity.com</span>
                    </div>

                </div>
                <div>
                    <div className="font-bold mb-2">Location</div>
                    <div className="text-gray-700">East Legon, Accra, Ghana</div>

                </div>
                <div>
                    <div className="font-bold mb-2">Delivery Radius</div>
                    <div className="text-gray-700">East Legon</div>

                </div>
                <div>
                    <div className="font-bold mb-2">Cost</div>
                    <div className="text-gray-700">₵ - Affordable</div>
                </div>
                <div>
                    <div className="font-bold mb-2">Food & Drink</div>
                    <div className="text-gray-700">Noodles and soft drinks</div>
                </div>
            </div>

            <div className="pb-10">
                <div className="font-bold mb-10">Customization</div>
                <div className="flex justify-center space-x-20">
                    <div className="flex flex-col text-left">
                        <div className="text-gray-700">Noodles(Mild or Spicy)</div>
                        <ol className="list-disc pl-8 leading-7 text-gray-700">
                            {noodles.map((noodle, index) => {
                                return (
                                    <li key={index}>{noodle}</li>
                                )

                            })}
                        </ol>
                    </div>
                    <div className="flex flex-col text-left">
                        <div className="text-gray-700">Protein</div>
                        <ol className="list-disc pl-8 leading-7 text-gray-700">

                            {proteins.map((protein, index) => {
                                return (
                                    <li key={index}>{protein}</li>
                                )

                            })}
                        </ol>
                    </div>
                    <div className="flex flex-col text-left">
                        <div className="text-gray-700">Vegetables</div>
                        <ol className="list-disc pl-8 leading-7 text-gray-700">

                            {vegetables.map((vegetable, index) => {
                                return (
                                    <li key={index}>{vegetable}</li>
                                )

                            })}
                        </ol>
                    </div>
                    <div className="flex flex-col text-left">
                        <div className="text-gray-700">Sauces</div>
                        <ol className="list-disc pl-8 leading-7 text-gray-700">

                            {sauces.map((sauce, index) => {
                                return (
                                    <li key={index}>{sauce}</li>
                                )

                            })}
                        </ol>
                    </div>
                </div>
            </div>

            <TwoGrid>
                <div className="pr-10">
                    <div className="font-bold mb-7">Main Target Audience</div>
                    <div className="max-w-xl">
                        <ol className="list-disc pl-8 leading-7 text-gray-700">
                            <li>College students that need food for lunch or dinner.</li>
                            <li>Busy working folks that do not have time to cook.</li>
                        </ol>
                    </div>
                </div>
                <div className="">
                    <div className="font-bold mb-7">Elevator Pitch</div>
                    <div className="leading-7 text-gray-700">
                        We provide tasty and affordable noodles in East Legon that can be customized to your
                        satisfaction. We deliver from 12 pm to 8pm every day.
                    </div>
                </div>
            </TwoGrid>
        </div>
    )
}

export default AppConceptnoodlescity;