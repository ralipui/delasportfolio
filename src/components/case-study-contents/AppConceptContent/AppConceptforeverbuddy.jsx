import React from "react";
import Title from "../../Features/Title";
import Square from "../../Features/Square";
import TwoGrid from "../../Features/TwoGrid";

const AppConceptforeverbuddy = () => {

    const squareItems = ["Easy access to office and school materials.", "Purchase products online.", "Receive assistance from 'Buddy.'"]
    const categories = ["Arts and Crafts", "Bags and Pencil Cases", "Binding and Cutting", "Boards and Easels",
        "Calendars and Planners", "Cash Handling", "Desk Accessories", "Drafting Supplies", "Education Supplies", "Envelopes and Forms"]
    const categories_ = ["Filing, Storage and Organizers", "Furniture", "Mailing, Shipping and Packaging", "Office Stationery",
        "Paper and Notebooks", "Presentation Supplies", "Printer Supplies", "Reading Books", "Technology", "Writing and Correction"]


    return (
        <div className="px-20 font-montserrat flex flex-col text-center space-y-10">
            <Title className="text-darkcyan">E-commerce Concept</Title>
            <div className="grid grid-cols-4 px-20 pb-8">
                <div>
                    <div className="font-bold mb-2">Name, URL & Tagline</div>
                    <div className="flex flex-col text-gray-700">
                        <span>Forever Buddy</span>
                        <span>foreverbuddy.com</span>
                        <span>"Making Learning Easier"</span>
                    </div>

                </div>
                <div>
                    <div className="font-bold mb-2"> Availability of Local Stores</div>
                    <div className="text-gray-700">40 African countries</div>

                </div>
                <div>
                    <div className="font-bold mb-2">Delivery Radius</div>
                    <div className="text-gray-700">All African countries</div>

                </div>
                <div>
                    <div className="font-bold mb-2">Cost</div>
                    <div className="text-gray-700">₵ - Free</div>
                </div>
            </div>

            <div className="pb-10">
                <div className="font-bold mb-10">Categories</div>
                <div className="flex justify-center space-x-10">
                    <div className="flex flex-col text-left">
                        <ol className="list-disc pl-8 leading-7 text-gray-700">

                            {categories.map((category, index) => {
                                return (
                                    <li key={index}>{category}</li>
                                )

                            })}
                        </ol>
                    </div>
                    <div className="flex flex-col text-left">
                        <ol className="list-disc pl-8 leading-7 text-gray-700">

                            {categories_.map((category, index) => {
                                return (
                                    <li key={index}>{category}</li>
                                )

                            })}
                        </ol>
                    </div>
                </div>
            </div>


            <div className="font-bold text-lg pb-6">Functions</div>

            <div className="flex space-x-16 justify-center pb-24">
                {squareItems.map((squareItem, index) => {
                    return <Square key={index} className="border-darkcyan">{squareItem}</Square>

                })}
            </div>

            <TwoGrid>
                <div className="pr-10">
                    <div className="font-bold mb-7">Main Target Audience</div>
                    <div className="max-w-xl">
                        <ol className="list-disc pl-8 leading-7 text-gray-700">
                            <li>Students that need school supplies for school.</li>
                            <li>Workers that need office supplies for work.</li>
                        </ol>
                    </div>
                </div>
                <div className="">
                    <div className="font-bold mb-7">Elevator Pitch</div>
                    <div className="leading-7 text-gray-700">
                        Buy affordable learning materials from the comfort of your home. We provide anything you need to
                        make learning easier for you.
                    </div>
                </div>
            </TwoGrid>
        </div>
    )

}

export default AppConceptforeverbuddy;