import React from "react";
import Title from "../../Features/Title";
import Square from "../../Features/Square";
import TwoGrid from "../../Features/TwoGrid";


const AppConceptTradecraft = () => {

    const squareItems = ["Easy access to handymen that are nearby or live in their area.", "Access to jobs\n" +
    "and business collaborations.", "Home improvement ideas and tips.", "Access to a community by sharing and discussing ideas."];


    return (
        <div className="px-20 font-montserrat flex flex-col text-center space-y-10">
            <Title className="text-maroon">App Concept</Title>
            <div className="grid grid-cols-3 px-20 pb-8">
                <div>
                    <div className="font-bold mb-2">App Name & Tagline</div>
                    <div className="flex flex-col text-gray-700">
                        <span>TradeCraft</span>
                        <span>"Make It Work"</span>
                    </div>

                </div>
                <div>
                    <div className="font-bold mb-2"> Availability of the app</div>
                    <div className="text-gray-700"> Ghana, West Africa</div>

                </div>
                <div>
                    <div className="font-bold mb-2">Cost</div>
                    <div className="text-gray-700">₵ - Free</div>

                </div>
            </div>
            <div className="font-bold text-lg pb-6">Functions</div>

            <div className="flex space-x-16 justify-center pb-24 text-gray-700">
                {squareItems.map((squareItem, index) => {
                    return <Square key={index} className="border-maroon">{squareItem}</Square>

                })}
            </div>

            <TwoGrid>
                <div className="pr-10">
                    <div className="font-bold mb-7">Main Target Audience</div>
                    <div className="max-w-xl">
                        <ol className="list-disc pl-8 leading-7 text-gray-700">
                            <li>People that are looking for handymen or home improvement companies to hire <span
                                className="font-bold">(buyers).</span></li>
                            <li>Handymen or home improvement companies that are looking for a platform to promote
                                themselves or their businesses <span className="font-bold">(sellers)</span>.
                            </li>
                        </ol>
                    </div>
                </div>
                <div className="">
                    <div className="font-bold mb-7">Elevator Pitch</div>
                    <div className="leading-7 text-gray-700">
                        Find and hire handymen or home improvement companies from the comfort of your homes. Better yet,
                        improve your home by getting inspired by the home improvement ideas the app provides.
                    </div>
                </div>
            </TwoGrid>
        </div>
    )
}

export default AppConceptTradecraft;