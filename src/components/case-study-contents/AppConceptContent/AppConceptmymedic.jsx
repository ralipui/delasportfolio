import React from "react";
import Title from "../../Features/Title";
import Square from "../../Features/Square";
import TwoGrid from "../../Features/TwoGrid";

const AppConceptmymedic = () => {

    const squareItems = ["Allows users to track their medications.", "Allows users to track other health related matters.", "Provides users with medication refill reminders.", "Provides a drug-to-drug interaction checker."];


    return (

        <div className="px-20 font-montserrat flex flex-col text-center space-y-10">
            <Title className="text-hotpink">App Concept</Title>
            <div className="grid grid-cols-3 px-20 pb-8">
                <div>
                    <div className="font-bold mb-2">App Name & Tagline</div>
                    <div className="flex flex-col text-gray-700">
                        <span>mymedic</span>
                        <span>"Track your Health"</span>
                    </div>

                </div>
                <div>
                    <div className="font-bold mb-2"> Availability of the app</div>
                    <div className="text-gray-700"> Ghana, West Africa</div>

                </div>
                <div>
                    <div className="font-bold mb-2">Cost</div>
                    <div className="text-gray-700">₵ - Free</div>

                </div>
            </div>
            <div className="font-bold text-lg pb-6">Functions</div>

            <div className="flex space-x-16 justify-center pb-24 text-gray-700">
                {squareItems.map((squareItem, index) => {
                    return <Square key={index} className="border-hotpink">{squareItem}</Square>

                })}
            </div>

            <TwoGrid>
                <div className="pr-10">
                    <div className="font-bold mb-7">Main Target Audience</div>
                    <div className="max-w-xl">
                        <ol className="list-disc pl-8 leading-7 text-gray-700">
                            <li>Elderly people with health problems.</li>
                            <li>People that have health conditions and constantly take medications.</li>
                        </ol>
                    </div>
                </div>
                <div className="">
                    <div className="font-bold mb-7">Elevator Pitch</div>
                    <div className="leading-7 text-gray-700">
                        Keep track of your health from the comfort of your home. Want to involve, use the mymedic
                        app to connect with your doctor for any medical assistance.
                    </div>
                </div>
            </TwoGrid>
        </div>

    )
}

export default AppConceptmymedic;