import React from "react";
import Title from "../../Features/Title";
import Square from "../../Features/Square";

const AppConceptsilverbird = () => {

    const squareItems = ["Purchase movie tickets.", "Reserve movie tickets.", "View movie details and cinema details."]

    return (
        <div className="px-20 font-montserrat flex flex-col text-center space-y-10">
            <Title className="text-deepskyblue">App Concept</Title>
            <div className="grid grid-cols-3 px-20 pb-8">
                <div>
                    <div className="font-bold mb-2">App Name</div>
                    <div className="flex flex-col text-gray-700">
                        <span>Silverbird Cinema</span>
                        <span>Movie Ticketing App</span>
                    </div>

                </div>
                <div>
                    <div className="font-bold mb-2"> Availability of the app</div>
                    <div className="text-gray-700"> Ghana & Nigeria, West Africa</div>

                </div>
                <div>
                    <div className="font-bold mb-2">Cost</div>
                    <div className="text-gray-700">₵ - Free</div>

                </div>
            </div>
            <div className="font-bold text-lg pb-6">Functions</div>

            <div className="flex space-x-16 justify-center pb-20 text-gray-700">
                {squareItems.map((squareItem, index) => {
                    return <Square key={index} className="border-deepskyblue">{squareItem}</Square>

                })}
            </div>

            <div className="pb-6 flex justify-center">
                <div>
                    <div className="font-bold mb-7">Main Target Audience</div>
                    <div className="max-w-2xl">
                        <ol className="list-disc pl-8 leading-7 text-gray-700">
                            <li>High school and college students who like to go to the movies to spend time with friends
                                or take a break from school.
                            </li>
                            <li>Busy young adults that want to take a break from work.</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default AppConceptsilverbird;