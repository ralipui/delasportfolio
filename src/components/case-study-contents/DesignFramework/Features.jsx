import React from "react";
import Title from "../../Features/Title";
import TwoGrid from "../../Features/TwoGrid";


const Features = (props) => {

    return (
        <div>
            <Title className={props.color}>Design Framework</Title>
            <div className="font-bold text-xl mb-16">Features</div>
            <TwoGrid>
                <div className="">
                    <div className="max-w-xl">
                        <ul className="list-disc pl-8 leading-7 text-gray-700">
                            {props.features_a}
                        </ul>
                    </div>
                </div>

                <div className="">
                    <div className="max-w-xl">
                        <ul className="list-disc pl-8 leading-7 text-gray-700">
                            {props.features_b}
                        </ul>
                        <ul className="list-[circle] pl-14 leading-7 text-gray-700">
                            {props.features_c}
                        </ul>
                    </div>
                </div>
            </TwoGrid>
        </div>
    )
}

export default Features;