import React from "react";


const InfoArchitecture = (props) => {

    return (
        <div className="mt-16 px-20">
            <div className="font-bold mb-12 text-xl">Information Architecture​</div>
            <div className="text-gray-700 mb-12 w-5/6 mx-auto">{props.arch_info}</div>
            <img src={props.sitemap} alt=""/>
        </div>
    )
}


export default InfoArchitecture;