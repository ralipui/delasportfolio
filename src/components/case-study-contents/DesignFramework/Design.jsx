import TwoGrid from "../../Features/TwoGrid";
import React from "react";

const Design = (props) => {

    return (
        <div className="mt-16 mb-16">
            <div className="font-bold text-xl mb-8">Design</div>
            <div className="text-gray-700 w-3/4 mx-auto">{props.design_info}</div>
            <TwoGrid className="mt-16">
                <div className="w-5/6">
                    <div className="font-bold mb-7">Mood board</div>
                    <div className="text-gray-700">
                        <div className="mb-8 leading-relaxed">{props.mood_info}
                        </div>
                    </div>
                </div>

                <div className="">
                    <img src={props.mood_img} alt=""/>

                </div>
            </TwoGrid>

            <TwoGrid className="mt-16">
                <div className="w-4/6">
                    <img src={props.logo_design_A} alt=""/>
                    <img src={props.logo_design_B} alt=""/>

                </div>
                <div className="w-5/6">
                    <div className="font-bold mb-7">Logo Design Process​</div>
                    <div className="text-gray-700">
                        <div className="mb-8 leading-relaxed">
                            {props.logo_process}
                        </div>
                    </div>
                </div>
            </TwoGrid>

            <div className="mt-16 px-20">
                <div className="font-bold mb-16 text-xl">Style Guide</div>
                <img src={props.style_guide} alt=""/>
            </div>
        </div>
    )
}

export default Design;