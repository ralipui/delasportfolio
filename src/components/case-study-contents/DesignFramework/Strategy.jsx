import React from "react";
import TwoGrid from "../../Features/TwoGrid";
import Title from "../../Features/Title";

const Strategy = (props) => {


    return (
        <div>
            <Title className={props.color}>Design Framework</Title>
            <div className="font-bold text-xl mb-10">Strategy</div>
            <TwoGrid>
                <div className="">
                    <div className="font-bold mb-7">User Needs</div>
                    <div className="max-w-xl">
                        <div className="text-gray-700">{props.user_needs_txt}</div>
                        <ul className="list-disc pl-8 leading-7 text-gray-700">
                            {props.user_needs}
                        </ul>
                    </div>
                </div>

                <div className="">
                    <div className="font-bold mb-7">Client Needs</div>
                    <div className="max-w-xl">
                        <div className="text-gray-700">{props.client_needs_txt}</div>
                        <ul className="list-disc pl-8 leading-7 text-gray-700">
                            {props.client_needs}
                        </ul>
                    </div>
                </div>
            </TwoGrid>
        </div>

    )

}

export default Strategy;