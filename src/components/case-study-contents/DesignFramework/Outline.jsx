import React from "react";
import TwoGrid from "../../Features/TwoGrid";

const Outline = (props) => {

    return (
        <TwoGrid className="mt-16">
            <div className="w-5/6">
                <div className="font-bold mb-7">Outline of Scope</div>
                <div className="text-gray-700">
                    <div className="mb-8 leading-relaxed">
                        {props.outline_txt}
                    </div>
                </div>
            </div>

            <div className="">
                <img src={props.outline_img} alt=""/>

            </div>
        </TwoGrid>
    )

}

export default Outline;