import React from "react";
import ListCircle from "../../Features/ListCircle";

const MymedicAudience = () => {

    return (
        <div className="grid grid-cols-3 px-20 pb-10">
            <div className="flex justify-center space-x-4">
                <span><ListCircle className="bg-hotpink"/></span>
                <span className="text-gray-700">Elderly people taking medications.</span>
            </div>
            <div className="flex justify-center space-x-4">
                <span><ListCircle className="bg-hotpink"/></span>
                <span className="text-gray-700">People that have health problems.</span>
            </div>
            <div className="flex">
                <span><ListCircle className="bg-hotpink"/></span>
                <span
                    className="text-gray-700t">People that constantly forget to take their medications.</span>

            </div>
        </div>
    )

}

export default MymedicAudience;

