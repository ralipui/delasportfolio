import React from "react";
import ListCircle from "../../Features/ListCircle";

const TradecraftAudience = () => {

    return (
        <div className="grid grid-cols-4 px-20 pb-10">
            <div className="flex">
                <span><ListCircle className="bg-maroon"/></span>
                <span className="text-gray-700">Handymen and home improvement companies.</span>
            </div>
            <div className="flex space-x-7">
                <span><ListCircle className="bg-maroon"/></span>
                <span className="text-gray-700">Creative individuals.</span>
            </div>
            <div className="flex">
                <span><ListCircle className="bg-maroon"/></span>
                <span className="text-gray-700">Money and time conscious people.</span>

            </div>
            <div className="flex space-x-6">
                <span><ListCircle className="bg-maroon"/></span>
                <span className="text-gray-700">Individuals looking for a side job as a handyman.</span>

            </div>
        </div>
    )

}

export default TradecraftAudience;