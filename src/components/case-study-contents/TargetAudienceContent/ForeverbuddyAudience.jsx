import React from "react";
import ListCircle from "../../Features/ListCircle";

const ForeverbuddyAudience = () => {

    return (
        <div className="grid grid-cols-4 px-20 pb-10">
            <div className="flex">
                <span><ListCircle className="bg-darkcyan"/></span>
                <span className="text-gray-700">Students that need school supplies.</span>
            </div>
            <div className="flex space-x-1.5">
                <span><ListCircle className="bg-darkcyan"/></span>
                <span className="text-gray-700">Workers that need office supplies.</span>
            </div>
            <div className="flex">
                <span><ListCircle className="bg-darkcyan"/></span>
                <span className="text-gray-700">People that are looking for
                                              affordable school and office supplies.</span>

            </div>
            <div className="flex">
                <span><ListCircle className="bg-darkcyan"/></span>
                <span className="text-gray-700">Parents that need learning supplies for their kids.</span>
            </div>
        </div>
    )
}

export default ForeverbuddyAudience;