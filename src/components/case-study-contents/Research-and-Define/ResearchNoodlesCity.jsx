import React from "react";
import Title from "../../Features/Title";
import UserResearch from "./UserResearch";
import Circle from "../../Features/Circle";
import UserPersonas from "./UserPersonas";
import person_1 from "../../../img/Noodles City/Artboard 1-100.jpg";
import person_2 from "../../../img/Noodles City/Artboard 3-100.jpg";
import user_journey_1 from "../../../img/Noodles City/Fafa's journey.jpg";
import user_journey_2 from "../../../img/Noodles City/Vanessa's journey.jpg";
import competitive_audit from "../../../img/Noodles City/References.jpg";
import CompetitiveAudit from "./CompetitiveAudit";

const circleItems = ["Cost", "Taste", "Quantity", "Roadside meals"];

const competAudit = ["Smooth ordering system", "High quality images", "Simple UI"];

const hypoItems = ["Users want a stress-free online ordering system.", "Users want easy online access to the foods and drinks sold on the website."]


const ResearchNoodlesCity = (props) => {
    return (

        <div className="px-20 font-montserrat flex flex-col text-center space-y-10 pb-16">
            <Title className={props.color}>Research & Define</Title>

            <UserResearch research={props.research}>
                {hypoItems.map((hypoItem, index) => {
                    return <li key={index}>{hypoItem}</li>
                })}
            </UserResearch>

            <div>
                <div className="font-bold text-xl pt-10 mb-4">Pain points</div>
                <div className="text-gray-700">Based on the responses, I identified the following pain points.</div>
                <div className="flex flex-col mt-12">
                    <div className="flex justify-between px-44">
                        {circleItems.map((circleItem, index) => {
                            return <Circle key={index} className="bg-tomato">{circleItem}</Circle>
                        })}
                    </div>
                    <div className="flex px-40 space-x-10 text-gray-700 mt-6">
                        <div className="w-4/6">

                            Meals in East Legon are expensive.
                        </div>
                        <div className="w-4/6">
                            Some of the meals are not appetizing.

                        </div>
                        <div className="w-4/6">
                            The meals are not sufficient but are still expensive.

                        </div>
                        <div className="w-4/6">
                            Roadside meals are normally cheap but they are unhealthy.
                        </div>

                    </div>
                </div>
            </div>

            <UserPersonas
                persona_info="Using the data collected, I created two user personas; one to represent college students, and the other to represent the working class. They present a realistic picture of the type of users that would use the Noodles City website."
                persona_1="Fafa is a student who needs to easy access affordable food online because they want to spend less on food."
                persona_2="Vanessa is a busy professional who needs easy access to sufficient and appetizing meals online because they do not have the time to cook."
                person_A={person_1} person_B={person_2}
            />


            <div className="pt-16 space-x-16">
                <div className="flex flex-col mx-auto max-w-3xl">
                    <div className="font-bold mb-7">Persona Journey Maps</div>
                    <div className="text-gray-700 leading-7 mb-16">I created journey maps for Fafa and Vanessa to show their journey when ordering for noodles. This would
                                                                   help me identify problems with the website and provide improvement opportunities to make the user
                                                                   experience better.
                    </div>
                </div>

                <div className="flex flex-row space-x-16">
                    <div className="">
                        <img src={user_journey_1} alt=""/>

                    </div>

                    <div className="pt-2">
                        <img src={user_journey_2} alt=""/>

                    </div>
                </div>
            </div>

            <div className="font-bold text-xl pt-20">Competitive Audit</div>
            <div className="text-gray-700 w-2/3 mx-auto">Below is a comparison of my restaurant idea to three other restaurants. I focused on the strengths and weaknesses
                                                         of these three restaurants. The first one is located in the United States, and the last two are located in Ghana.
            </div>


            <img src={competitive_audit} alt="" className="px-32"/>

            <div>
                <div className="font-bold text-xl pt-10 mb-16">Competitive Audit Insights</div>
                <div className="flex flex-col mt-12">
                    <div className="flex justify-between px-56">
                        {competAudit.map((circleItem, index) => {
                            return <Circle key={index} className="bg-tomato">{circleItem}</Circle>
                        })}
                    </div>
                    <div className="flex px-40 text-gray-700 mt-6">
                        <div className="w-1/4 mx-auto">
                            An ordering system that would be easy to navigate.
                        </div>
                        <div className="w-1/4 mx-auto">
                            Use of high quality images that would be pleasing to users and bring them back for more.

                        </div>
                        <div className="w-1/4 mx-auto">
                            Simple UI that involves the use of good color combination, playful fonts, and pleasing images.

                        </div>
                    </div>
                </div>
            </div>

            <CompetitiveAudit comp_title_first="1. Personal Account" comp_title_second="2. Online Ordering system"
                              title_first_content="Users need an account to track, save or edit their orders. It also allows them to review their order history."
                              title_second_content="To order online in Ghana, the contact information is provided for the customer to call and place their order. But from my research, I noticed that Noodles and company, which is the only foreign company I looked at, provides an online ordering system that makes life easier for the customer. So, I decided to apply that to my project."

            />

        </div>

    )
}

export default ResearchNoodlesCity;