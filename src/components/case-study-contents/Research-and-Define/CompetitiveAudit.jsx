import React from "react";

const CompetitiveAudit = (props) => {
    return (
        <div>
            <div className="font-bold text-xl pt-10 mb-16">Competitive Audit Ideas</div>
            <div className="grid grid-cols-2 text-left">
                <div className="pr-20">
                    <div className="font-bold mb-7">{props.comp_title_first}</div>
                    <div className="leading-7 text-gray-700">{props.title_first_content}</div>
                </div>

                <div className="">
                    <div className="font-bold mb-7">{props.comp_title_second}</div>
                    <div className="leading-7 text-gray-700">{props.title_second_content}</div>
                </div>
            </div>
        </div>
    )


}

export default CompetitiveAudit;