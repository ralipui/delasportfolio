import React from "react";
import Title from "../../Features/Title";
import UserResearch from "./UserResearch";
import Circle from "../../Features/Circle";
import UserPersonas from "./UserPersonas";
import empathy_1 from "../../../img/Forever Buddy/Empathy Map.jpg";
import empathy_2 from "../../../img/Forever Buddy/Empathy Map 2.jpg";
import person_1 from "../../../img/Forever Buddy/Persona.jpg";
import person_2 from "../../../img/Forever Buddy/Artboard 2-100.jpg";
import user_journey_1 from "../../../img/Forever Buddy/Nii's Journey.jpg";
import user_journey_2 from "../../../img/Forever Buddy/Danielle's Journey.jpg";
import competitive_audit from "../../../img/Forever Buddy/Competitve audit-1.jpg";
import CompetitiveAudit from "./CompetitiveAudit";

const circleItems = ["Uniqueness and lack of variety", "Cost", "Availability"];

const competAudit = ["Offer gift cards and discounts", "Worldwide delivery", "Organized Interface"];

const hypoItems = ["Easy product search especially with the use of search filters.", "Detailed information on products.", "Product images; large images, 360 views, close shots, and product videos.", "Easy checkout process.",
    "Product reviews that help users have an idea of the product they are purchasing.", "Providing customers with shopping assistance using the Buddy concept."]


const ResearchForeverBuddy = (props) => {

    return (
        <div className="px-20 font-montserrat flex flex-col text-center space-y-10 pb-16">
            <Title className={props.color}>Research & Define</Title>

            <UserResearch research={props.research}>
                {hypoItems.map((hypoItem, index) => {
                    return <li key={index}>{hypoItem}</li>
                })}
            </UserResearch>
            <div>
                <img src={empathy_1} alt=""/>
                <img src={empathy_2} alt="" className="pl-3"/>
            </div>


            <div>
                <div className="font-bold text-xl pt-10 mb-4">Pain points</div>
                <div className="text-gray-700">Based on the responses, I identified the following pain points.</div>
                <div className="flex flex-col mt-12">
                    <div className="flex justify-between px-56">
                        {circleItems.map((circleItem, index) => {
                            return <Circle key={index} className="bg-darkcyan">{circleItem}</Circle>
                        })}
                    </div>
                    <div className="flex px-40 space-x-10 text-gray-700 mt-6">
                        <div className="w-4/6">
                            Products lack uniqueness in terms of appearance.
                        </div>
                        <div className="w-4/6">
                            Products are expensive and poor quality.

                        </div>
                        <div className="w-4/6">
                            Some products are not available in the country and if they are, they are hard to find.

                        </div>

                    </div>
                </div>
            </div>

            <UserPersonas
                persona_info="This part of the project focuses on two user personas I created based on the data I collected. The first user persona is Nii who represents the adults that use the website. And Danielle is the second user persona, represents the teens that would use the website."
                persona_1="Nii is a busy professional who needs easy access to unique and high quality stationery online because they prefer using trendy products."
                persona_2="Danielle is a student who needs easy access to school supplies (arts and crafts) online because they struggle to find supplies."
                person_A={person_1} person_B={person_2}
            />


            <div className="pt-16 space-x-16">
                <div className="flex flex-col mx-auto max-w-3xl">
                    <div className="font-bold mb-7">Persona Journey Maps</div>
                    <div className="text-gray-700 leading-7 mb-16"> I created journey maps for Nii and Danielle, but their journeys are slightly different. Nii's journey
                                                                    would use the normal search flow while Danielle's journey would involve using the 'Buddy' system to
                                                                    assist her in her search.
                    </div>
                </div>

                <div className="flex flex-row space-x-16">
                    <div className="">
                        <img src={user_journey_1} alt=""/>

                    </div>

                    <div className="pt-2">
                        <img src={user_journey_2} alt=""/>

                    </div>
                </div>
            </div>

            <div className="font-bold text-xl pt-20">Competitive Audit - SWOT Analysis</div>
            <div className="text-gray-700 w-2/3 mx-auto">Analysing the market is very important because it would help us know the opportunities that are available to help
                                                         Forever Buddy thrive. For this part of the project, I would assess the strenghts, weaknesses of the 3 companies
                                                         above; Jumia, Amazon and Alibaba. And based on the strenghts, weaknesses, I would identify the threats they pose
                                                         to Forever Buddy, and the opportunities that are available for Forever Buddy.
            </div>


            <img src={competitive_audit} alt=""/>

            <div>
                <div className="font-bold text-xl pt-10 mb-16">Competitive Audit Insights</div>
                <div className="flex flex-col mt-12">
                    <div className="flex justify-between px-56">
                        {competAudit.map((circleItem, index) => {
                            return <Circle key={index} className="bg-darkcyan">{circleItem}</Circle>
                        })}
                    </div>
                    <div className="flex px-40 text-gray-700 mt-6">
                        <div className="w-1/4 mx-auto">
                            Offer discounts and gift cards, especially to students since the whole point is to provide affordable products.
                        </div>
                        <div className="w-1/4 mx-auto">
                            Even though Forever Buddy is currently focusing on Africa, delivering worldwide can provide access to a larger target market.

                        </div>
                        <div className="w-1/4 mx-auto">
                            Provide users with important features like recommendations, new releases, etc. But also making sure to keep the site organized.

                        </div>
                    </div>
                </div>
            </div>

            <CompetitiveAudit comp_title_first="1. Location" comp_title_second="2. The Buddy System"
                              title_first_content="For the other websites, the user can select a language, currency and the country they want their product to be shipped to. Based on this, I have decided that users would be able to select their preferred language and currency. They would also be able to enter their location by selecting their preferred African country. And for countries like Ghana which is divided into regions, the user would also be able to select their region."
                              title_second_content="This is a concept I have decided to include in the website which is inspired by Alibaba’s personalization concept. The buddy section of the website would assist users in finding the right products they need for a particular subject. E.g., When a user asks Buddy ‘Materials needed for a painting class,’ they would be given a list of products they would need for a painting class. This would make it easier for the user to find all the products needed for the painting class."

            />

        </div>


    )

}

export default ResearchForeverBuddy;