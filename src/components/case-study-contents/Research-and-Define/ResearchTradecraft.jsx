import React from "react";
import Title from "../../Features/Title";
import UserResearch from "./UserResearch";
import Circle from "../../Features/Circle";
import UserPersonas from "./UserPersonas";
import person_1 from "../../../img/TradeCraft/Artboard 1-100.jpg";
import person_2 from "../../../img/TradeCraft/Artboard 2-100.jpg";
import user_journey from "../../../img/TradeCraft/User Journey Map-1.jpg";
import competitive_audit from "../../../img/TradeCraft/Competitve audit-1.jpg";
import CompetitiveAudit from "./CompetitiveAudit";

const circleItems = ["Location", "Qualification", "Cost", "DIY"];

const competAudit = ["Simple System", "Accessibility"];

const hypoItems = ["Users want easy online access to home improvement services.", "Users want easy online access to customers that need their services."]


const ResearchTradecraft = (props) => {

    return (
        <div className="px-20 font-montserrat flex flex-col text-center space-y-10 pb-16">
            <Title className={props.color}>Research & Define</Title>

            <UserResearch research={props.research_item}>
                {hypoItems.map((hypoItem, index) => {
                    return <li key={index}>{hypoItem}</li>
                })}
            </UserResearch>
            {props.affinity}

            <div>
                <div className="font-bold text-xl pt-10 mb-4">Pain points</div>
                <div className="text-gray-700">Based on the responses, I identified the following pain points.</div>
                <div className="flex flex-col mt-12">
                    <div className="flex justify-between px-40">
                        {circleItems.map((circleItem, index) => {
                            return <Circle key={index} className="bg-maroon">{circleItem}</Circle>
                        })}
                    </div>
                    <div className="flex px-36 space-x-10 text-gray-700 mt-6">
                        <div className="w-4/6">
                            A lot of handymen and clients live far which becomes an inconvenience.
                        </div>
                        <div className="w-4/6">
                            Some handymen are not qualified and this affects time and money.

                        </div>
                        <div className="w-4/6">
                            High prices of services because they live far so they have to consider their transport
                            when charging clients.

                        </div>
                        <div className="w-4/6">
                            Access to DIY ideas makes it easier to fix small issues without the help of outsiders.

                        </div>
                    </div>
                </div>
            </div>

            <UserPersonas persona_info="Based on the data I gathered from the interviews, I created two user personas; Richard who represents the
                                                               handymen (sellers), and Naa who represents the people looking to hire (buyers)."
                          persona_1="Richard is a busy professional and interior design lover who needs to easily access home
                                                                        improvement ideas online because they want to improve their trade and design skills."
                          persona_2="Naa is a busy professional who needs easy access to qualified handymen online because they do not
                                                                        want to waste their time and money."
                          person_A={person_1} person_B={person_2}
            />


            <div className="flex pt-16 space-x-16">
                <div className="flex flex-col text-left w-2/6">
                    <div className="font-bold mb-7">Naa's Journey Map</div>
                    <div className="text-gray-700 leading-7 mb-16"> I created a journey map for only the buyer (Naa) because the final design of the app would focus
                                                                    on the buyer. The journey map shows the process Naa would go through when searching for a
                                                                    qualified handyman.
                    </div>
                </div>

                <div className="">
                    <img src={user_journey} alt=""/>

                </div>
            </div>

            <div className="font-bold text-xl pt-20">Competitive Audit</div>
            <div className="text-gray-700 w-2/3 mx-auto">In Ghana, there is no competition that would affect the use of the TradeCraft app, but there are other apps
                                                         that serve a similar purpose in other countries. The purpose of this audit is to identify the features in
                                                         these similar apps and why they were used and why.
            </div>


            <img src={competitive_audit} alt=""/>

            <div>
                <div className="font-bold text-xl pt-10 mb-16">Competitive Audit Insights</div>
                <div className="flex flex-col mt-12">
                    <div className="flex justify-between px-[22rem]">
                        {competAudit.map((circleItem, index) => {
                            return <Circle key={index} className="bg-maroon">{circleItem}</Circle>
                        })}
                    </div>
                    <div className="flex px-60 text-gray-700 mt-6">
                        <div className="w-2/6 mx-auto">
                            A lot of handymen and clients live far which becomes an inconvenience.
                        </div>
                        <div className="w-2/6 mx-auto">
                            Some handymen are not qualified and this affects time and money.

                        </div>
                    </div>
                </div>
            </div>

            <CompetitiveAudit comp_title_first="1. Personal Account" comp_title_second="2. Social Feature"
                              title_first_content="The app enables the user to find and hire handymen and home improvement companies without an account.
                                  They can also explore home décor and improvement ideas. However, I
                        decided that users should have personal accounts if they want to be a part of the
                        TradeCraft community. The account will allow them to follow others and be followed, chat
                        with other users, upload content, and view purchase history." title_second_content="Even though the app is a business app, I was inspired by the Houzz mobile app decided to
                        make my app more fun to use by including a
                        social feature where users can follow and follow other users, and also communicate with
                        other users."
            />

        </div>

    )
}

export default ResearchTradecraft;