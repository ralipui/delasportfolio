import React from "react";

const UserPersonas = (props) => {

    return (
        <div>
            <div className="font-bold text-xl pt-20 mb-4">User Personas</div>
            <div className="text-gray-700 w-2/3 mx-auto mb-20">{props.persona_info}</div>

            <div className="flex flex-col text-left space-y-10">
                <div className="flex flex-row space-x-24">
                    <div className="basis-1/4">
                        <div className="font-bold mb-7">Problem Statement</div>
                        <div className="text-gray-700 leading-7 mb-16">{props.persona_1}</div>
                    </div>

                    <div className="basis-3/4">
                        <img src={props.person_A} className="" alt=""/>
                    </div>
                </div>

                {/*space-x-24*/}

                <div className="flex flex-row space-x-24 mt-20">
                    <div className="basis-1/4">
                        <div className="font-bold mb-7">Problem Statement</div>
                        <div className="text-gray-700 leading-7 mb-16">{props.persona_2}</div>
                    </div>

                    <div className="basis-3/4">
                        <img src={props.person_B} className="" alt=""/>

                    </div>
                </div>
            </div>
        </div>

    )

}

export default UserPersonas;

