import React from "react";
import Title from "../../Features/Title";
import Circle from "../../Features/Circle";
import UserPersonas from "./UserPersonas";
import graph_1 from "../../../img/Mymedic/graph_1.png";
import graph_A from "../../../img/Mymedic/graph_A.png";
import graph_B from "../../../img/Mymedic/graph_B.png";
import graph_C from "../../../img/Mymedic/graph_C.png";
import graph_D from "../../../img/Mymedic/graph_D.png";
import affinity from "../../../img/Mymedic/Affinity_Map.jpg";
import person_1 from "../../../img/Mymedic/Persona_1.jpg";
import person_2 from "../../../img/Mymedic/Persona_2.jpg";
import competitive_audit from "../../../img/Mymedic/Competitve_audit.jpg";
import CompetitiveAudit from "./CompetitiveAudit";
import TwoGrid from "../../Features/TwoGrid";


const circleItems = ["Forgetfulness", "Unclear Instructions", "Work", "Natural Medicine", "Ineffective Medicine"];

const competAudit = ["Data Authentication", "No Ads", "Language Options"];


const ResearchMedic = (props) => {

    return (
        <div className="px-20 font-montserrat flex flex-col text-center space-y-10 pb-16">
            <Title className={props.color}>Research & Define</Title>

            <div className="pb-10">
                <div className="font-bold text-xl mb-8">Secondary Research</div>
                <div className="leading-7 text-gray-700 max-w-4xl mx-auto">
                    This part of the project focuses on primary research I conducted by interviewing participants. And secondary research that focuses on factors that
                    influence medications non-adherence among hypertensive patients in Ghana. The focus on hypertensive patients stems from the fact that hypertension is
                    high in sub-Saharan African countries like Ghana.
                </div>
            </div>

            <TwoGrid>
                <div className="pr-20 my-auto">
                    <div className="leading-7 text-gray-700">
                        As stated earlier, medication adherence is the extent to which patients take their medications and follow medical advice. Non adherence occurs
                        when a patient does not correctly follow medical advice and irregularly follow medical prescriptions (Atinga et al., 2018).
                        According to Atinga et al. (2018), The main reason for medication non adherence among diabetes and hypertensive patients in Ghana was the
                        perception that medications were ineffective to solve the underlying health issue. This lead patients to rely on herbal medicines and spiritual
                        healing because of affordability, accessibility and efficacy.
                        Other factors that were identified to influence medication non adherence were tight work schedules, poor health prescriptions by health health
                        providers, social norms amongst others (Atinga et al., 2018).
                    </div>
                </div>

                <div className="">
                    <img src={graph_1} alt=""/>
                </div>
            </TwoGrid>

            <div className="pb-10">
                <div className="leading-7 text-gray-700 max-w-4xl mx-auto">
                    Research done by Boima et al. (2015) focuses on factors associated with medication non-adherence among hypertensives in Ghana and Nigeria. Based on
                    their findings, the following factors are associated with medication non-adherence among hypertensives in Ghana and Nigeria: age, herbal preparations,
                    formal education, and health insurance. The charts below provides a rough estimation of the results to paint a clearer picture of the results.
                </div>
            </div>

            <div>
                <div className="flex flex-row">
                    <div className="">
                        <img src={graph_A} alt=""/>
                    </div>
                    <div className="">
                        <img src={graph_B} alt=""/>
                    </div>
                    <div className="">
                        <img src={graph_C} alt=""/>
                    </div>
                    <div className="">
                        <img src={graph_D} alt=""/>
                    </div>
                </div>
                <div className="flex space-x-10 text-gray-700 mt-6">
                    <div className="w-3/6">
                        A lot of handymen and clients live far which becomes an inconvenience.
                    </div>
                    <div className="w-4/6">
                        Some handymen are not qualified and this affects time and money.

                    </div>
                    <div className="w-4/6">
                        High prices of services because they live far so they have to consider their transport
                        when charging clients.

                    </div>
                    <div className="w-4/6">
                        Access to DIY ideas makes it easier to fix small issues without the help of outsiders.

                    </div>
                </div>
            </div>


            <div className="flex flex-row space-x-24 pt-20">
                <div className="basis-1/4 text-left my-auto">
                    <div className="font-bold mb-7">Primary Research</div>
                    <div className="text-gray-700 leading-7">To understand why people are non adherent to medications based on my own small research for the app, I
                                                             interviewed 4 participants; 2 young adults and 2 elderly people. Based on the results, I created an
                                                             affinity map by grouping participants' pain points into themes.
                    </div>
                </div>

                <div className="basis-3/4">
                    <img src={affinity} className="" alt=""/>
                </div>
            </div>


            <div>
                <div className="font-bold text-xl pt-10 mb-6">Pain points</div>
                <div className="text-gray-700 pb-10">Based on the responses, I identified the following pain points.</div>
                <div className="flex flex-col mt-12">
                    <div className="flex justify-between px-5">
                        {circleItems.map((circleItem, index) => {
                            return <Circle key={index} className="bg-hotpink">{circleItem}</Circle>
                        })}
                    </div>
                    <div className="flex space-x-10 text-gray-700 mt-6">
                        <div className="w-4/6">
                            Participants stated that they forget to take their medications because they are preoccupied.
                        </div>
                        <div className="w-4/6">
                            Some doctors or health providers have poor hand-writings that make it difficult for the patient to understand the instructions.
                        </div>
                        <div className="w-4/6">
                            Busy work schedules cause forgetfulness which leads to medication non-adherence.
                        </div>
                        <div className="w-4/6">
                            Natural medicines are safer than the man-made medications.
                        </div>
                        <div className="w-4/6">
                            Medications sometimes do not provide positive results.
                        </div>

                    </div>
                </div>
            </div>

            <UserPersonas
                persona_info="Based on the results of my interview, I created two personas to further empathize with users by understanding their needs. The two personas are Joseph and Lena. Joseph represents the elderly, and Lena represents the young adults."
                persona_1="Joseph is a busy professional who needs to be reminded on when to take his medications because he does not want to keep forgetting to take his medications."
                persona_2="Lena is a student and babysitter who needs to easily track the medications of the kids she babysits because she does not want to risk anything bad happening to the kids."
                person_A={person_1} person_B={person_2}
            />


            <div className="font-bold text-xl pt-20">Competitive Audit</div>
            <div>
                <div className="text-gray-700 w-2/3 mx-auto">This part of my project analysis the competition as a way to produce the best version of the app. The audit
                                                             focuses on the strengths and weaknesses of competitors app, as well as the features that make each app
                                                             similar or
                                                             different.
                </div>


                <img src={competitive_audit} alt=""/>
            </div>

            <div className="pb-16">
                <div className="font-bold text-xl pt-10 mb-16">Competitive Audit Insights</div>
                <div className="flex flex-col mt-12">
                    <div className="flex justify-between px-56">
                        {competAudit.map((circleItem, index) => {
                            return <Circle key={index} className="bg-hotpink">{circleItem}</Circle>
                        })}
                    </div>
                    <div className="flex px-32 text-gray-700 mt-6">
                        <div className="w-1/4 mx-auto">
                            Ensure that users data is protected.
                        </div>
                        <div className="w-1/4 mx-auto">
                            Prevent ads to ensure a smooth experience for users.
                        </div>
                        <div className="w-1/4 mx-auto">
                            Provide the app in different language so it is accessible to everyone.
                        </div>
                    </div>
                </div>
            </div>

            <CompetitiveAudit comp_title_first="1. Multiple profiles" comp_title_second="2. Sharing"
                              title_first_content="This feature would allow users to create profiles for more than one person making it possible to track the medications and other health matters of multiple individuals."
                              title_second_content="This feature would allow users to share their health plan or diary with other people. Additional features would be accessible depending on who the data is shared with."

            />

        </div>
    )

}

export default ResearchMedic;