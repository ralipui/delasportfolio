import React from "react";
import Title from "../../Features/Title";
import UserResearch from "./UserResearch";
import Circle from "../../Features/Circle";
import UserPersonas from "./UserPersonas";
import person_1 from "../../../img/Silverbird/Persona 1 (1).jpg";
import person_2 from "../../../img/Silverbird/Persona 2 (1).jpg";
import user_journey_1 from "../../../img/Silverbird/User_Journey_Map_1.jpg";
import user_journey_2 from "../../../img/Silverbird/User_Journey_Map_2.jpg";
import competitive_audit from "../../../img/Silverbird/Competitve audit (1).jpg";
import CompetitiveAudit from "./CompetitiveAudit";

const circleItems = ["Time", "Money", "Location"];

const competAudit = ["Offer the app in different languages", "Smooth ordering process"];

const hypoItems = ["Users want to easily purchase movie tickets online.", "Users want to easily reserve movie tickets online."]


const ResearchSilverbird = (props) => {

    return (
        <div className="px-20 font-montserrat flex flex-col text-center space-y-10 pb-16">
            <Title className={props.color}>Research & Define</Title>

            <UserResearch research={props.research}>
                {hypoItems.map((hypoItem, index) => {
                    return <li key={index}>{hypoItem}</li>
                })}
            </UserResearch>
            {props.affinity}

            <div>
                <div className="font-bold text-xl pt-10 mb-4">Pain points</div>
                <div className="text-gray-700">Based on the responses, I identified the following pain points.</div>
                <div className="flex flex-col mt-12">
                    <div className="flex justify-between px-56">
                        {circleItems.map((circleItem, index) => {
                            return <Circle key={index} className="bg-deepskyblue">{circleItem}</Circle>
                        })}
                    </div>
                    <div className="flex px-40 space-x-10 text-gray-700 mt-6">
                        <div className="w-4/6">
                            Travelling to the cinema and waiting in line is
                            time consuming.
                        </div>
                        <div className="w-4/6">
                            Travelling to the cinema to purchase a movie
                            ticket that is not available is a waste of time and money.

                        </div>
                        <div className="w-4/6">
                            The cinema is too far to purchase a ticket in advance.

                        </div>

                    </div>
                </div>
            </div>

            <UserPersonas
                persona_info="Based on the user research, I created 2 personas; Michelle and Nana Kwame to better empathize with users and understand their needs.
                Michelle represents the high school and college students, and Nana Kwame represents the young adults that want to take a break from work."
                persona_1="Michelle is a student and movie lover who needs a convenient way to check the availability of movie tickets and purchase movie tickets because she wastes money when she travels to the movie theatre and tickets are not available."
                persona_2="Nana Kwame is a busy professional who needs an easier way to purchase movie tickets because he does not want to waste his time in queues at the cinema."
                person_A={person_1} person_B={person_2}
            />


            <div className="pt-16 space-x-16">
                <div className="flex flex-col mx-auto max-w-3xl">
                    <div className="font-bold mb-7">Persona Journey Maps</div>
                    <div className="text-gray-700 leading-7 mb-16"> The images below show journey maps for Michelle and Nana Kwame. Michelle's journey map focuses on her
                                                                    purchase of a movie ticket. And Nana Kwame's journey map focuses on his reservation of a movie ticket.
                    </div>
                </div>

                <div className="flex flex-row">
                    <div className="basis-1/2">
                        <img src={user_journey_1} alt=""/>

                    </div>

                    <div className="basis-1/2">
                        <img src={user_journey_2} alt=""/>

                    </div>
                </div>
            </div>

            <div className="font-bold text-xl pt-20">Competitive Audit</div>
            <div className="text-gray-700 w-2/3 mx-auto">This part of my project focuses on the competitors of Silverbird Cinema. The audit focuses on the strengths and
                                                         weaknesses of competitors app, as well as the quality of their product. Doing this audit would give me a better
                                                         understanding of Silverbird's competitors and how it can position itself in the market.
            </div>


            <img src={competitive_audit} alt=""/>

            <div>
                <div className="font-bold text-xl pt-10 mb-16">Competitive Audit Insights</div>
                <div className="flex flex-col mt-12">
                    <div className="flex justify-between px-[22rem]">
                        {competAudit.map((circleItem, index) => {
                            return <Circle key={index} className="bg-deepskyblue">{circleItem}</Circle>
                        })}
                    </div>
                    <div className="flex px-60 text-gray-700 mt-6">
                        <div className="w-2/6 mx-auto">
                            This would make the app accessible to both local and foreign users.
                        </div>
                        <div className="w-2/6 mx-auto">
                            Ensuring a smooth ordering process is vital so users are comfortable using the app.

                        </div>
                    </div>
                </div>
            </div>

            <CompetitiveAudit comp_title_first="1. Offer discounts" comp_title_second="2. Add a food shop"
                              title_first_content="Offer discounts and gift cards which would include movie and food packages."
                              title_second_content="Allow users to access food shops online so they can order from the comfort of the cinema."

            />

        </div>


    )

}

export default ResearchSilverbird;