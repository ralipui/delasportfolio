import React from "react";
import TwoGrid from "../../Features/TwoGrid";

const UserResearch = (props) => {
    return (
        <TwoGrid>
            <div className="pr-20">
                <div className="font-bold mb-7">User Research</div>
                <div className="leading-7 text-gray-700">
                    {props.research}
                </div>
            </div>

            <div className="">
                <div className="font-bold mb-7">Hypotheses</div>
                <div className="max-w-xl">
                    <ol className="list-disc pl-8 leading-7 text-gray-700">
                        {props.children}
                    </ol>
                </div>
            </div>
        </TwoGrid>
    )

}

export default UserResearch;