import React from "react";
import Socials from "../Features/Socials";

function Footer() {
    const year = new Date().getFullYear()

    return (
        <section className="">
            <hr/>
            <div className="px-20">
                <div className="flex space-x-14 font-montserrat py-16 justify-center text-lg">
                    <button rel="noreferrer">Works</button>
                    <button rel="noreferrer">Resume</button>
                </div>
                <div className="flex justify-center pb-28">
                    <Socials/>
                </div>
                <div className="flex-col text-center py-8 text-grey font-montserrat text-sm">
                    <p className="mb-2">Copyright © {year} Dela Alipui </p>
                    <p>Developed by Richard Alipui </p>
                </div>


            </div>
        </section>
    )
}

export default Footer;