import React from "react"
import wave from "../../img/wave.png";
import dp from "../../img/Website_dp.png";


//Work in the icon size

function Home() {


    return (
        <section className="pt-48">
            <div className="absolute w-full bottom-4/5 mt-8">
                <div className="flex flex-col text-center">
                    <span className="font-montserrat font-extralight text-9xl">Dela Alipui</span>
                    <span className="font-regular font-montserrat mt-5 text-salmon text-5xl">
                        UI/UX Designer
                </span>
                </div>
            </div>

        </section>
    )

}

function About() {
    return (
        <section className="pt-48">
            <div className="absolute w-full bottom-4/5 mt-8">
                <div className="font-montserrat flex flex-col text-center">
                    <div className="flex flex-row mx-auto">
                        <span className="font-normal text-4xl mb-10 mx-auto">Hi there, I'm Dela </span>
                        <span className="ml-4"><img className="w-3/5" src={wave} alt="wave emoji"/></span>
                    </div>
                    <span className="w-4/6 mx-auto leading-6">As a <span className="text-salmon">UX designer</span> with a good background in <span
                        className="text-salmon">visual design and user research</span>, I am skilled at <span className="text-salmon">creating engaging</span> and <span
                        className="text-salmon">intuitive user experiences.</span> I have a strong understanding of <span
                        className="text-salmon">usability principles</span> and <span className="text-salmon">user-centered design.</span> In my current role, I have been able to <span
                        className="text-salmon">lead design projects from start to finish</span>, collaborating closely with <span className="text-salmon">cross-functional teams</span> to ensure that our products meet the <span
                        className="text-salmon">highest standards</span> of usability and design. I am <span className="text-salmon">passionate</span> about using
                                       <span className="text-salmon"> technology to positively impact society</span>. It brings me so much joy to know that I can <span
                            className="text-salmon">make people's lives easier</span> through my knowledge and skills
                    </span>
                </div>

            </div>

        </section>
    )
}


function NavBottom() {

    const styleHover = "hover:text-salmon cursor-pointer duration-500";

    return (
        <section>
            <div className="flex font-montserrat absolute w-full bottom-52">
                <div className="flex flex-row mx-auto gap-x-10">
                    <a href="" className="visited:text-salmon">
                        <div className={styleHover}>Me</div>
                    </a>
                    <a href="">
                        <div className={styleHover}>About</div>
                    </a>
                    <div className={styleHover}>Skills</div>
                    <div className={styleHover}>Experience</div>
                </div>
            </div>


        </section>

    )
}

{/*<div className="w-3/6 mx-auto">*/
}
{/*    <img src={dp} alt="Dela's picture"/>*/
}
{/*</div>*/
}


function YearsExperience() {
    return (

        <div className="flex justify-center">
            <div className="flex flex-row">
                <div className="grid grid-rows-2 mt-24 pb-8 mx-auto">
                    <div className="mx-auto">
                        <h1 className="text-7xl font-semibold mx-auto">1+</h1>
                    </div>

                    <div className="mx-auto">
                        <p className="mt-8 font-semibold text-grey text-lg font-montserrat">Years of Experience</p>
                    </div>
                </div>

                <div className="my-auto pt-10 max-w-4xl ">
                    <p className="mt-8 font-thin text-3xl leading-10 pr-12 pl-24 font-montserrat ml-16">Product
                                                                                                        Designer
                                                                                                        focused on
                                                                                                        UI/UX,
                                                                                                        Branding,
                                                                                                        Logo Design
                                                                                                        and Graphic
                                                                                                        Design.</p>
                </div>
            </div>
        </div>

    )

}

export default Home;
export {YearsExperience, NavBottom, About}

