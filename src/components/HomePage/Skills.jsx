import React from "react";


function Skills() {


    return (
        <section className="pt-48">
            <div className="absolute w-full bottom-4/5 mt-6">
                <div className="font-montserrat flex flex-col">
                    <DesignSkills/>
                    {/*<SoftwareSkills/>*/}

                </div>
                <CarouselSkills/>

            </div>

        </section>

    )
}

function DesignSkills() {
    const design_skills = ["Visual design", "Mobile app design", "Storyboarding", "Usability Testing", "User experience", "Wireframing", "User research", "Information Architecture", "Web design", "Prototyping", "Responsive design"]

    return (
        <section className="mx-auto">
            <div className="font-normal text-4xl mb-10 text-center">Design</div>
            <div className="mb-12">
                <div className="font-montserrat flex">
                    <div className="grid grid-rows-3 gap-4 grid-cols-4">
                        {design_skills.map(function (design) {
                            return (
                                <div className="flex">
                                    <span className="h-1 w-1 bg-black rounded-full my-auto mr-2"></span>
                                    <span>
                                    {design}
                                </span>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        </section>

    )
}

function SoftwareSkills() {
    const software_skills = ["Figma", "Adobe Photoshop", "InVision", "Adobe XD", "Balsamiq", "Adobe illustrator", "Indesign", "Webflow"]

    return (
        <section className="mx-auto">
            <div className="font-normal text-4xl mb-10 text-center">Software</div>
            <div className="mb-12">
                <div className="font-montserrat flex">
                    <div className="grid grid-rows-3 gap-4 gap-x-14 grid-cols-3">
                        {software_skills.map(function (software) {
                            return (
                                <div className="flex">
                                    <span className="h-1 w-1 bg-black rounded-full my-auto mr-2"></span>
                                    <span>
                                    {software}
                                </span>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        </section>

    )
}


function CarouselSkills() {
    return (

        <div className="bottom-0 flex relative w-full">
            <div className="flex flex-row gap-x-4 mx-auto">
                <span><hr className="w-12 border-t-salmon border-t-4"/></span>
                <span><hr className="w-12 border-t-salmon border-t-4 opacity-50"/></span>
            </div>
        </div>

    )
}


export default Skills;
