import React from "react";
import back_to_top from "../../img/back_to_top.png"

const year = new Date().getFullYear();

const buttonClick = () => {
    return (
        window.scrollTo({top: 0, left: 0, behavior: 'smooth'})
    )
}

function Contact() {

    return (
        <section>
            <hr className=""/>

            <div className="flex-col px-20 text-center space-y-8 py-20">
                <div className="font-normal font-montserrat text-4xl text-salmon">
                    Let's Work Together 😊
                </div>
                <div className="font-montserrat text-lg max-w-3xl mx-auto">
                    <span className="font-bold">Email:</span> delalipui@gmail.com
                </div>

            </div>
            <div className="px-20 flex mb-10">
                <img src={back_to_top} alt="back to top" className="w-12 ml-auto cursor-pointer hover:animate-bounce" onClick={buttonClick}/>
            </div>

            <NewFooter/>

            <hr/>
            
        </section>
    )
}

function NewFooter() {
    return (
        <div className="w-full mt-10">
            <div className="flex relative bottom-4 text-center font-montserrat text-gray-400 w-full">
                <div className="flex flex-col justify-center lg:flex-row lg:gap-x-5 text-xs 2xl:tracking-wide w-full">
                    <span id="year">&copy; {year} Dela Alipui. All right reserved.</span>
                    <span className="h-2 w-2 bg-salmon rounded-full my-auto"></span>
                    <span>Developed by Richard Alipui.</span>

                </div>

            </div>
        </div>
    )

}


export default Contact;
export {NewFooter}