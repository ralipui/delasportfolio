import React, {useState} from "react";
import logo from "../../img/Dela-2.png"
import Socials from "../Features/Socials";


function Nav() {

    const [action, setaction] = useState(false);

    function changeText() {
        setaction(true);

    }

    function unchangeText() {
        setaction(false);

    }


    return (
        <nav className="bg-white shadow-sm w-full fixed">
            <div className="px-20">
                <div className="flex justify-between py-4">
                    <div className="my-auto space-x-8 font-normal text-gray-700">
                        <a href="#">Works</a>
                        <a href="#">Resume</a>
                        <a href="#" className="" onMouseOver={changeText}
                           onMouseLeave={unchangeText}>{action ?
                            <span className="text-salmon">delalipui@gmail.com</span> :
                            <span className="">Contact</span>}</a>
                    </div>
                    <div>
                        <img className="w-28" src={logo} alt="logo"/>

                    </div>

                    <Socials/>
                </div>
            </div>
        </nav>
    )
}

export default Nav;