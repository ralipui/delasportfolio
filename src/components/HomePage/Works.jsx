import React from "react";


function Works() {

    return (
        <section className="">
            <hr className="mt-9"/>

            <div className="bg-red-100 px-20 pb-40">
                <div className="bg-yellow-500 text-center mt-24 mb-20">
                    <p className="font-semibold font-montserrat text-4xl">Works</p>
                </div>


                <div>
                    <div className="flex gap-x-20 text-3xl text-salmon">
                        <div className="">
                            <button className="">Case Studies</button>
                        </div>
                        <button>Branding</button>
                    </div>
                </div>
                <div>
                    <div className="case-study-container flex space-x-6 mt-20">
                        <div className="bg-green-300 w-2/4">image</div>
                        <div className="bg-blue-200 p-10 w-1/3 flex-col space-y-6">
                            <h2 className="font-montserrat font-bold tracking-wide text-xl">Silverbird Cinema App
                                                                                            Design</h2>
                            <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tortor, elementum ullamcorper
                                 ac
                                 risus id platea phasellus. Sed bibendum eget a dolor elit semper.
                            </div>
                            <button
                                className="bg-salmon p-4 rounded-md text-white tracking-wider font-montserrat">View
                                                                                                               Case
                                                                                                               Study
                            </button>
                        </div>
                    </div>

                    <div className="case-study-image-selectors mt-20 flex space-x-6">
                        <button className="bg-red-400 w-1/5 h-36">image1</button>
                        <button className="bg-yellow-400 w-1/5 h-36">image2</button>
                        <button className="bg-green-400 w-1/5 h-36">image3</button>
                        <button className="bg-blue-400 w-1/5 h-36">image4</button>
                    </div>
                </div>
            </div>
        </section>

    )
}


export default Works;